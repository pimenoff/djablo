
#include "DjabloCharacterBase.h"
#include "Djablo.h"
#include "DjabloHealthComponent.h"
#include <Components/CapsuleComponent.h>
#include <GameFramework/Controller.h>
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>
#include <GameFramework/CharacterMovementComponent.h>


ADjabloCharacterBase::ADjabloCharacterBase(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
	PrimaryActorTick.bCanEverTick = true;

	HealthComponent = CreateDefaultSubobject<UDjabloHealthComponent>("HealthComponent");
}

void ADjabloCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	check(HealthComponent);

	check(GetCharacterMovement());
	check(GetCapsuleComponent());
	check(GetMesh());

	OnHealthChanged(HealthComponent->GetHealth(), 0.0f);
	HealthComponent->OnDeath.AddUObject(this, &ADjabloCharacterBase::OnDeath);
	HealthComponent->OnHealthChanged.AddUObject(this, &ADjabloCharacterBase::OnHealthChanged);
}

void ADjabloCharacterBase::OnHealthChanged(float Health, float HealthDelta) {}

void ADjabloCharacterBase::OnDeath()
{
	GetCharacterMovement()->DisableMovement();
	SetLifeSpan(5.0f);

	GetCapsuleComponent()->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Ignore);

	GetMesh()->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	GetMesh()->SetSimulatePhysics(true);

	if (DeathSound) {
		UGameplayStatics::PlaySound2D(GetWorld(), DeathSound);
	}

	FinalizeDeath();
}