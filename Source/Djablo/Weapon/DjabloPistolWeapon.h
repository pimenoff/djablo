#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWeapon.h"
#include "DjabloPistolWeapon.generated.h"


UCLASS()
class DJABLO_API ADjabloPistolWeapon : public ADjabloBaseWeapon
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float Damage = 10.f;

	virtual void BeginPlay() override;
	virtual void MakeShot() override;
	void DoDamage(FHitResult HitResult);

private:


};
