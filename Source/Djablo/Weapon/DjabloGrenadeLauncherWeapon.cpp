#include "DjabloGrenadeLauncherWeapon.h"
#include "DjabloProjectile.h"
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>


void ADjabloGrenadeLauncherWeapon::StartFire()
{
	MakeShot();
}

void ADjabloGrenadeLauncherWeapon::MakeShot()
{
	const auto World = GetWorld();
	if (!World) {
		return;
	}

	if (IsAmmoEmpty() && NoAmmoSound) {
		UGameplayStatics::PlaySound2D(World, NoAmmoSound);
		return;
	}

	FVector TraceStart, TraceEnd;
	GetTraceData(TraceStart, TraceEnd);

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	const auto EndPoint = HitResult.bBlockingHit ? HitResult.ImpactPoint : TraceEnd;

	//if (HitResult.bBlockingHit) {
	//	DrawDebugLine(World, SocketTransform.GetLocation(), HitResult.ImpactPoint, FColor::Red, false, 3.f, 0, 5.f);
	//	DrawDebugSphere(World, HitResult.ImpactPoint, 10.f, 24, FColor::Red, false, 5.f);
	//	DoDamage(HitResult);
	//}
	//else {
	//	DrawDebugLine(World, SocketTransform.GetLocation(), TraceEnd, FColor::Red, false, 3.f, 0, 5.f);
	//}

	const FTransform SocketTransform = WeaponMesh->GetSocketTransform(MuzzleSocketName);
	const FVector Direction = (EndPoint - SocketTransform.GetLocation()).GetSafeNormal();
	const FTransform SpawnTransform(FRotator::ZeroRotator, SocketTransform.GetLocation());
	auto Projectile = GetWorld()->SpawnActorDeferred<ADjabloProjectile>(ProjectileClass, SpawnTransform);

	if (Projectile) {
		Projectile->SetShotDirection(Direction);
		Projectile->SetOwner(GetOwner());
		Projectile->FinishSpawning(SpawnTransform);
	}

	DecreaseAmmo();
	SpawnMuzzleFX();

	if (FireSound) {
		UGameplayStatics::PlaySound2D(World, FireSound);
	}
}