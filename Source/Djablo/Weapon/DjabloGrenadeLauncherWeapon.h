#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWeapon.h"
#include "DjabloGrenadeLauncherWeapon.generated.h"


class ADjabloProjectile;


UCLASS()
class DJABLO_API ADjabloGrenadeLauncherWeapon : public ADjabloBaseWeapon
{
	GENERATED_BODY()

	virtual void StartFire() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ADjabloProjectile> ProjectileClass;

	virtual void MakeShot() override;
};
