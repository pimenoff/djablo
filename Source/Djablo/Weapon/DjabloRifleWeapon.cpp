#include "DjabloRifleWeapon.h"
#include "DjabloWeaponFXComponent.h"
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/DamageEvents.h>


void ADjabloRifleWeapon::StartFire()
{
	GetWorldTimerManager().SetTimer(ShotTimerHandler, this, &ADjabloRifleWeapon::MakeShot, ShotPeriod, true);
	MakeShot();
}

void ADjabloRifleWeapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(ShotTimerHandler);
}

void ADjabloRifleWeapon::MakeShot()
{
	const auto World = GetWorld();
	if (!World) {
		return;
	}

	if (IsAmmoEmpty() && NoAmmoSound) {
		StopFire();
		UGameplayStatics::PlaySound2D(World, NoAmmoSound);
		return;
	}

	FVector TraceStart, TraceEnd;
	GetTraceData(TraceStart, TraceEnd, BulletSpread);

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	if (HitResult.bBlockingHit) {
		DoDamage(HitResult);
		WeaponFXComponent->PlayImpactFX(HitResult);
		WeaponFXComponent->SpawnTraceFX(GetMuzzleWorldLocation(), HitResult.ImpactPoint);
	}

	DecreaseAmmo();
	SpawnMuzzleFX();

	if (FireSound) {
		UGameplayStatics::PlaySound2D(World, FireSound);
	}
}

void ADjabloRifleWeapon::DoDamage(FHitResult HitResult)
{
	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor) {
		return;
	}

	DamagedActor->TakeDamage(Damage, FDamageEvent(), GetPlayerController(), this);
}
