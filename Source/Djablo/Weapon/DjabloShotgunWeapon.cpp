#include "DjabloShotgunWeapon.h"
#include "DjabloWeaponFXComponent.h"
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/DamageEvents.h>


void ADjabloShotgunWeapon::StartFire()
{
	MakeShot();
}

void ADjabloShotgunWeapon::MakeShot()
{
	const auto World = GetWorld();
	if (!World) {
		return;
	}

	if (IsAmmoEmpty() && NoAmmoSound) {
		UGameplayStatics::PlaySound2D(World, NoAmmoSound);
		return;
	}

	for (int i = 0; i < ParticlesInShot; i++) {
		FVector TraceStart, TraceEnd;
		GetTraceData(TraceStart, TraceEnd, BulletSpread);

		FHitResult HitResult;
		MakeHit(HitResult, TraceStart, TraceEnd);

		if (HitResult.bBlockingHit) {
			DoDamage(HitResult);
			WeaponFXComponent->PlayImpactFX(HitResult);
			WeaponFXComponent->SpawnTraceFX(GetMuzzleWorldLocation(), HitResult.ImpactPoint);
		}
	}

	DecreaseAmmo();
	SpawnMuzzleFX();

	if (FireSound) {
		UGameplayStatics::PlaySound2D(World, FireSound);
	}
}

void ADjabloShotgunWeapon::DoDamage(FHitResult HitResult)
{
	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor) {
		return;
	}

	DamagedActor->TakeDamage(Damage, FDamageEvent(), GetPlayerController(), this);
}
