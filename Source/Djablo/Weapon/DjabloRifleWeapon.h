#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWeapon.h"
#include "DjabloRifleWeapon.generated.h"


UCLASS()
class DJABLO_API ADjabloRifleWeapon : public ADjabloBaseWeapon
{
	GENERATED_BODY()

public:
	virtual void StartFire() override;
	virtual void StopFire() override;

protected:

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float TimerBetweenShoot = 0.1f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float BulletSpread = 1.5f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	float Damage = 10.f;

	virtual void MakeShot() override;
	void DoDamage(FHitResult HitResult);
};
