#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Actor.h>
#include "DjabloCoreTypes.h"
#include "DjabloBaseWeapon.generated.h"


class USkeletalMeshComponent;
class UParticleSystemComponent;
class UDjabloWeaponFXComponent;
class USoundCue;


UCLASS()
class DJABLO_API ADjabloBaseWeapon : public AActor
{
	GENERATED_BODY()

public:
	ADjabloBaseWeapon();

	FOnClipsEmptySignature OnClipsEmpty;
	FAmmoData GetAmmoData() const { return CurrentAmmo; }
	USkeletalMeshComponent* GetWeaponMesh() { return WeaponMesh; }

	virtual void StartFire();
	virtual void StopFire();

	virtual void ChangeClip();
	virtual bool CanReload() const;
	virtual bool TryToAddAmmo(int32 ClipsAmount);
	virtual bool IsAmmoEmpty() const;
	virtual bool IsAmmoFull() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Components")
	USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FName MuzzleSocketName = "MuzzleFlash";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float ShotPeriod = 0.1f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FAmmoData DefaultAmmo {10, 10, false};

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* NoAmmoSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* FireSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	UParticleSystem* MuzzleFX;

	UPROPERTY(VisibleAnywhere, Category = "FX")
	UDjabloWeaponFXComponent* WeaponFXComponent;

	FTimerHandle ShotTimerHandler;

	virtual void BeginPlay() override;
	virtual void MakeShot();
	virtual void DecreaseAmmo();
	virtual bool IsClipEmpty() const;
	virtual void MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd);
	UParticleSystemComponent* SpawnMuzzleFX();
	AController* GetPlayerController();
	FVector GetMuzzleWorldLocation() const;
	virtual void GetTraceData(FVector& TraceStart, FVector& TraceEnd, float BulletSpreadDegrees = 0.f) const;
	void GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const;
	FVector RandSector(FVector const& Dir, float HorizontalConeHalfAngleRad) const;

	float TraceMaxDistance = 100000.f;

private:
	FAmmoData CurrentAmmo;
};
