#include "DjabloProjectile.h"
#include "DjabloWeaponFXComponent.h"
#include <Components/SphereComponent.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include <DrawDebugHelpers.h>
#include <Kismet/GameplayStatics.h>


ADjabloProjectile::ADjabloProjectile()
{
    PrimaryActorTick.bCanEverTick = true;

    CollisionComponent = CreateDefaultSubobject<USphereComponent>("SphereComponent");
    CollisionComponent->InitSphereRadius(10.f);
    CollisionComponent->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
    CollisionComponent->SetCollisionResponseToAllChannels(ECollisionResponse::ECR_Block);
    CollisionComponent->bReturnMaterialOnMove = true;
    SetRootComponent(CollisionComponent);

    ProjectileMovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>("ProjectileMovementComponent");
    WeaponFXComponent = CreateDefaultSubobject<UDjabloWeaponFXComponent>("WeaponFXComponent");
}

void ADjabloProjectile::BeginPlay()
{
    Super::BeginPlay();

    check(ProjectileMovementComponent);
    check(CollisionComponent);
    check(WeaponFXComponent);

    ProjectileMovementComponent->Velocity = ShotDirection * ProjectileMovementComponent->InitialSpeed;
    CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
    CollisionComponent->OnComponentHit.AddDynamic(this, &ADjabloProjectile::OnHit);
    //CollisionComponent->IgnoreActorWhenMoving(GetOwner(), true);
    SetLifeSpan(LifeSpan);
}

void ADjabloProjectile::OnHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& HitResult)
{
    const auto World = GetWorld();
    if (!World) {
        return;
    }

    ProjectileMovementComponent->StopMovementImmediately();

    const auto Pawn = Cast<APawn>(GetOwner());
    const auto Controller = Pawn ? Pawn->GetController() : nullptr;

    UGameplayStatics::ApplyRadialDamage(
        World,
        DamageAmount,
        GetActorLocation(),
        DamageRadius,
        UDamageType::StaticClass(),
        { GetOwner() },
        this,
        Controller,
        true
    );
    //DrawDebugSphere(GetWorld(), GetActorLocation(), DamageRadius, 24, FColor::Red, false, 1.0f);
    WeaponFXComponent->PlayImpactFX(HitResult);

    if (ImpactSound) {
        UGameplayStatics::PlaySound2D(World, ImpactSound);
    }

    Destroy();
}