#include "DjabloBaseWeapon.h"
#include "DjabloWeaponFXComponent.h"
#include <Components/SkeletalMeshComponent.h>
#include <Engine/World.h>
#include <DrawDebugHelpers.h>
#include <GameFramework/Character.h>
#include <GameFramework/Controller.h>
#include <Particles/ParticleSystem.h>
#include <NiagaraFunctionLibrary.h>
#include <NiagaraComponent.h>
#include <Kismet/GameplayStatics.h>


ADjabloBaseWeapon::ADjabloBaseWeapon()
{
	PrimaryActorTick.bCanEverTick = false;

	WeaponMesh = CreateDefaultSubobject<USkeletalMeshComponent>("WeaponMesh");
	SetRootComponent(WeaponMesh);

	WeaponFXComponent = CreateDefaultSubobject<UDjabloWeaponFXComponent>("WeaponFXComponent");
}

void ADjabloBaseWeapon::BeginPlay()
{
	Super::BeginPlay();

	CurrentAmmo = DefaultAmmo;
}

void ADjabloBaseWeapon::StartFire() {}
void ADjabloBaseWeapon::StopFire() {}
void ADjabloBaseWeapon::MakeShot() {}

AController* ADjabloBaseWeapon::GetPlayerController()
{
	const auto Pawn = Cast<APawn>(GetOwner());
	if (!Pawn) {
		return nullptr;
	}

	const auto Controller = Pawn->GetController();
	if (!Controller) {
		return nullptr;
	}

	return Controller;
}

FVector ADjabloBaseWeapon::GetMuzzleWorldLocation() const
{
    return WeaponMesh->GetSocketLocation(MuzzleSocketName);
}

void ADjabloBaseWeapon::GetTraceData(FVector& TraceStart, FVector& TraceEnd, float BulletSpreadDegrees) const
{
	FVector ViewLocation;
	FRotator ViewRotation;

	GetPlayerViewPoint(ViewLocation, ViewRotation);

	TraceStart = ViewLocation;
	FVector ShootDirection;
	if (BulletSpreadDegrees > 0) {
		//const auto HalfRand = FMath::DegreesToRadians(BulletSpreadDegrees);
		//FMath::VRandCone()
		ShootDirection = RandSector(ViewRotation.Vector(), BulletSpreadDegrees);
	}
	else {
		ShootDirection = ViewRotation.Vector();
	}
	 
	TraceEnd = TraceStart + ShootDirection * TraceMaxDistance;
}

FVector ADjabloBaseWeapon::RandSector(FVector const& Dir, float HorizontalConeHalfAngleRad) const
{
	if (HorizontalConeHalfAngleRad > 0.f) {
		FMatrix const DirMat = FRotationMatrix(Dir.Rotation());
		FVector const DirZ = DirMat.GetScaledAxis(EAxis::Z);

		FVector Result = Dir.RotateAngleAxis(HorizontalConeHalfAngleRad * (FMath::FRand() - 0.5), DirZ);

		Result = Result.GetSafeNormal();

		return Result;
	}
	else {
		return Dir.GetSafeNormal();
	}
}

void ADjabloBaseWeapon::GetPlayerViewPoint(FVector& ViewLocation, FRotator& ViewRotation) const
{
    ViewLocation = GetMuzzleWorldLocation();
    ViewRotation = WeaponMesh->GetSocketRotation(MuzzleSocketName);
}

void ADjabloBaseWeapon::MakeHit(FHitResult& HitResult, const FVector& TraceStart, const FVector& TraceEnd)
{
	if (!GetWorld()) {
		return;
	}

	FCollisionQueryParams CollisionParams;
	CollisionParams.AddIgnoredActor(GetOwner());
	CollisionParams.bReturnPhysicalMaterial = true;

	GetWorld()->LineTraceSingleByChannel(
		HitResult,
		TraceStart,
		TraceEnd,
		ECollisionChannel::ECC_Pawn,
		CollisionParams
	);
}

void ADjabloBaseWeapon::DecreaseAmmo()
{
	if (CurrentAmmo.Bullets == 0) {
		return;
	}

	CurrentAmmo.Bullets--;

	if (IsClipEmpty() && !IsAmmoEmpty()) {
		StopFire();
		OnClipsEmpty.Broadcast(this);
	}
}

bool ADjabloBaseWeapon::CanReload() const
{
	return CurrentAmmo.Bullets < DefaultAmmo.Bullets && CurrentAmmo.Clips > 0;
}

bool ADjabloBaseWeapon::IsAmmoEmpty() const
{
	return !CurrentAmmo.Infinite && CurrentAmmo.Clips == 0 && IsClipEmpty();
}

bool ADjabloBaseWeapon::IsClipEmpty() const
{
	return CurrentAmmo.Bullets == 0;
}

void ADjabloBaseWeapon::ChangeClip()
{
	if (!CurrentAmmo.Infinite) {
		if (CurrentAmmo.Clips == 0) {
			return;
		}

		CurrentAmmo.Clips--;
	}

	CurrentAmmo.Bullets = DefaultAmmo.Bullets;
}

bool ADjabloBaseWeapon::IsAmmoFull() const
{
	return CurrentAmmo.Clips == DefaultAmmo.Clips && //
		CurrentAmmo.Bullets == DefaultAmmo.Bullets;
}

bool ADjabloBaseWeapon::TryToAddAmmo(int32 ClipsAmount)
{
	if (CurrentAmmo.Infinite || IsAmmoFull() || ClipsAmount <= 0) {
		return false;
	}

	if (IsAmmoEmpty()) {
		CurrentAmmo.Clips = FMath::Clamp(ClipsAmount, 0, DefaultAmmo.Clips + 1);
		OnClipsEmpty.Broadcast(this);
	}
	else if (CurrentAmmo.Clips < DefaultAmmo.Clips) {
		const auto NextClipsAmount = CurrentAmmo.Clips + ClipsAmount;
		if (DefaultAmmo.Clips - NextClipsAmount >= 0) {
			CurrentAmmo.Clips = NextClipsAmount;
		}
		else {
			CurrentAmmo.Clips = DefaultAmmo.Clips;
			CurrentAmmo.Bullets = DefaultAmmo.Bullets;
		}
	}
	else {
		CurrentAmmo.Bullets = DefaultAmmo.Bullets;
	}

	return true;
}

UParticleSystemComponent* ADjabloBaseWeapon::SpawnMuzzleFX()
{
	//return UNiagaraFunctionLibrary::SpawnSystemAttached(MuzzleFX, //
	//	WeaponMesh, //
	//	MuzzleSocketName, //
	//	FVector::ZeroVector, //
	//	FRotator::ZeroRotator, //
	//	EAttachLocation::SnapToTarget,
	// true
	//);
	return UGameplayStatics::SpawnEmitterAttached(
		MuzzleFX,
		WeaponMesh,
		MuzzleSocketName,
		FVector::ZeroVector,
		FRotator::ZeroRotator,
		EAttachLocation::SnapToTarget,
		true
	);
}