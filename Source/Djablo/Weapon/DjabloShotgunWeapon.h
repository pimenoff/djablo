#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWeapon.h"
#include "DjabloShotgunWeapon.generated.h"


UCLASS()
class DJABLO_API ADjabloShotgunWeapon : public ADjabloBaseWeapon
{
	GENERATED_BODY()

	virtual void StartFire() override;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float BulletSpread = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	int32 ParticlesInShot = 5;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	float Damage = 10.f;

	virtual void MakeShot() override;
	void DoDamage(FHitResult HitResult);
};
