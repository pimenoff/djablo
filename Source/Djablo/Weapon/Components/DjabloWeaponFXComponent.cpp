#include "DjabloWeaponFXComponent.h"
#include <NiagaraFunctionLibrary.h>
#include <NiagaraComponent.h>
#include <PhysicalMaterials/PhysicalMaterial.h>
#include <Components/DecalComponent.h>
#include <Kismet/GameplayStatics.h>


UDjabloWeaponFXComponent::UDjabloWeaponFXComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}


void UDjabloWeaponFXComponent::PlayImpactFX(const FHitResult& Hit)
{
	auto ImpactData = DefaultImpactData;

	if (Hit.PhysMaterial.IsValid()) {
		const auto PhysMat = Hit.PhysMaterial.Get();
		if (ImpactDataMap.Contains(PhysMat)) {
			ImpactData = ImpactDataMap[PhysMat];
		}
	}

	if (ImpactData.NiagaraEffect) {
		UNiagaraFunctionLibrary::SpawnSystemAtLocation(
			GetWorld(),
			ImpactData.NiagaraEffect,
			Hit.ImpactPoint,
			Hit.ImpactNormal.Rotation()
		);
	}

	if (ImpactData.DecalData.Material) {
		auto Decal = UGameplayStatics::SpawnDecalAtLocation(
			GetWorld(),
			ImpactData.DecalData.Material,
			ImpactData.DecalData.Size,
			Hit.ImpactPoint,
			Hit.ImpactNormal.Rotation()
		);

		if (Decal) {
			Decal->SetFadeOut(ImpactData.DecalData.LifeTime, ImpactData.DecalData.FadeOutTime);
		}
	}
}


void UDjabloWeaponFXComponent::SpawnTraceFX(FVector TraceStart, FVector TraceEnd)
{
	const auto TraceFXComponent = UNiagaraFunctionLibrary::SpawnSystemAtLocation(
		GetWorld(),
		TraceFX,
		TraceStart
	);

	if (TraceFXComponent) {
		TraceFXComponent->SetNiagaraVariableVec3(TraceTargetName, TraceEnd);
	}
}