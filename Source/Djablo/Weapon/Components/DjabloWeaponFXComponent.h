#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>
#include "DjabloCoreTypes.h"
#include "DjabloWeaponFXComponent.generated.h"


class UPhysicalMaterial;
class UNiagaraSystem;


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DJABLO_API UDjabloWeaponFXComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UDjabloWeaponFXComponent();

	void PlayImpactFX(const FHitResult& Hit);
	void SpawnTraceFX(FVector TraceStart, FVector TraceEnd);

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	FImpactData DefaultImpactData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	TMap<UPhysicalMaterial*, FImpactData> ImpactDataMap;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	UNiagaraSystem* TraceFX;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "FX")
	FString TraceTargetName = "TraceTarget";
};
