#include "DjabloPistolWeapon.h"
#include "DjabloWeaponFXComponent.h"
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>
#include <Engine/DamageEvents.h>


void ADjabloPistolWeapon::BeginPlay()
{
	Super::BeginPlay();

	check(WeaponFXComponent);
}

void ADjabloPistolWeapon::StartFire()
{
	MakeShot();
}

void ADjabloPistolWeapon::MakeShot()
{
	const auto World = GetWorld();
	if (!World) {
		return;
	}

	if (IsAmmoEmpty() && NoAmmoSound) {
		UGameplayStatics::PlaySound2D(World, NoAmmoSound);
		return;
	}

	FVector TraceStart, TraceEnd;
	GetTraceData(TraceStart, TraceEnd);

	FHitResult HitResult;
	MakeHit(HitResult, TraceStart, TraceEnd);

	if (HitResult.bBlockingHit) {
		//DrawDebugLine(World, ViewLocation, HitResult.ImpactPoint, FColor::Red, false, 3.f, 0, 5.f);
		//DrawDebugSphere(World, HitResult.ImpactPoint, 10.f, 24, FColor::Red, false, 5.f);
		DoDamage(HitResult);
		WeaponFXComponent->PlayImpactFX(HitResult);
		WeaponFXComponent->SpawnTraceFX(GetMuzzleWorldLocation(), HitResult.ImpactPoint);
	}
	else {
		//DrawDebugLine(World, ViewLocation, TraceEnd, FColor::Red, false, 3.f, 0, 5.f);
	}

	DecreaseAmmo();
	SpawnMuzzleFX();

	if (FireSound) {
		UGameplayStatics::PlaySound2D(World, FireSound);
	}
}

void ADjabloPistolWeapon::DoDamage(FHitResult HitResult)
{
	const auto DamagedActor = HitResult.GetActor();
	if (!DamagedActor) {
		return;
	}

	DamagedActor->TakeDamage(Damage, FDamageEvent(), GetPlayerController(), this);
}

