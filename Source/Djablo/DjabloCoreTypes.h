#pragma once

#include "DjabloCoreTypes.generated.h"


class ADjabloBaseWeapon;

DECLARE_MULTICAST_DELEGATE_OneParam(FOnClipsEmptySignature, ADjabloBaseWeapon*);

DECLARE_MULTICAST_DELEGATE(FOnDeathSignature);
DECLARE_MULTICAST_DELEGATE_TwoParams(FOnHealthChangedSignature, float, float);


UENUM(BlueprintType)
enum EWeaponName {
	Pistol          UMETA(DisplayName = "Pistol"),
	Rifle           UMETA(DisplayName = "Rifle"),
	Shotgun         UMETA(DisplayName = "Shotgun"),
	GrenadeLauncher UMETA(DisplayName = "Grenade Launcher"),
};


USTRUCT(BlueprintType)
struct FAmmoData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	int32 Bullets;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (EditCondition = "!Infinite"))
	int32 Clips;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	bool Infinite;
};


USTRUCT(BlueprintType)
struct FWeaponData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ADjabloBaseWeapon> WeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	UAnimMontage* ReloadAnimMontage;
};

USTRUCT(BlueprintType)
struct FDecalData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UMaterialInterface* Material;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FVector Size = FVector(10.f);

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	float LifeTime = 10.f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	float FadeOutTime = 1.f;
};


class UNiagaraSystem;
class USoundCue;

USTRUCT(BlueprintType)
struct FImpactData
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	UNiagaraSystem* NiagaraEffect;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "VFX")
	FDecalData DecalData;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* Sound;
};

UENUM(BlueprintType)
enum EGameState
{
	Paused       UMETA(DisplayName = "Paused"),
	InProgress   UMETA(DisplayName = "In Progress"),
	GameOverWon  UMETA(DisplayName = "Game Over Won"),
	GameOverLost UMETA(DisplayName = "Game Over Lost"),
};

DECLARE_MULTICAST_DELEGATE_OneParam(FOnGameStateChangedSignature, EGameState);
DECLARE_MULTICAST_DELEGATE(FOnLevelClearedSignature);

UENUM(BlueprintType)
enum EWalkerIdleState {
	Standup        UMETA(DisplayName = "Standup"),
	StandingUp     UMETA(DisplayName = "Standing Up"),
	Sleeping       UMETA(DisplayName = "Sleeping"),
	Eating         UMETA(DisplayName = "Eating"),
	Agonyzing      UMETA(DisplayName = "Agonyzing"),
	Crawling       UMETA(DisplayName = "Crawling"),
	CrawlingAmbush UMETA(DisplayName = "CrawlingAmbush"),
};


UENUM(BlueprintType)
enum EThrowerIdleState {
	Idle         UMETA(DisplayName = "Idle"),
	Melee        UMETA(DisplayName = "Melee"),
	Throw        UMETA(DisplayName = "Throw"),
	RangedAttack UMETA(DisplayName = "RangedAttack"),
};


USTRUCT(BlueprintType)
struct FRingInt
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon", meta = (DefaultValue = 0, ClampMin = 0))
	int32 InnerRadius = 0;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	int32 OuterRadius = 100;
};