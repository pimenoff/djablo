#pragma once

#include <CoreMinimal.h>
#include <GenericTeamAgentInterface.h>
#include <GameFramework/PlayerController.h>
#include "DjabloCoreTypes.h"
#include "DjabloPlayerController.generated.h"


UCLASS()
class DJABLO_API ADjabloPlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

private:
	FGenericTeamId GetGenericTeamId() const { return FGenericTeamId(0); };
	void OnGameStateChanged(EGameState GameState);
	void SetInputModeGameOnly(bool InConsumeCaptureMouseDown);
};
