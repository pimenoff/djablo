#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimNotifies/AnimNotify.h>
#include "DjabloReloadFinishedAnimNotify.generated.h"


DECLARE_MULTICAST_DELEGATE_OneParam(FOnNotifiedSignature, USkeletalMeshComponent*);


UCLASS()
class DJABLO_API UDjabloReloadFinishedAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) override;

	FOnNotifiedSignature OnNotified;
};
