#pragma once

#include <CoreMinimal.h>
#include <GameFramework/GameModeBase.h>
#include "DjabloCoreTypes.h"
#include "DjabloGameMode.generated.h"


UCLASS()
class DJABLO_API ADjabloGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	FOnLevelClearedSignature OnLevelCleared;
	FOnGameStateChangedSignature OnGameStateChanged;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite)
	int TotalEnemiesOnLevel = 0;

	virtual bool SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate = FCanUnpause()) override;
	virtual bool ClearPause() override;

	void IncrementKillsNum();
	void GameOver(bool IsWon);

	int KillsNum;

protected:
	virtual void BeginPlay() override;
	virtual void StartPlay() override;

private:
	TEnumAsByte<EGameState> GameState;
};
