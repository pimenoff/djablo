#include "DjabloPlayerCharacter.h"
#include "DjabloWeaponComponent.h"
#include "DjabloBaseWeapon.h"
#include "DjabloGameMode.h"
#include "Djablo.h"
#include <AbilitySystemComponent.h>
#include <Camera/CameraComponent.h>
#include <Components/CapsuleComponent.h>
#include <Components/InputComponent.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Controller.h>
#include <GameFramework/GameModeBase.h>
#include <GameFramework/SpringArmComponent.h>
#include <EnhancedInputComponent.h>
#include <EnhancedInputSubsystems.h>
#include <DrawDebugHelpers.h>
#include <InputActionValue.h>
#include <Kismet/GameplayStatics.h>


ADjabloPlayerCharacter::ADjabloPlayerCharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	GetCharacterMovement()->bOrientRotationToMovement = true;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 500.0f, 0.0f);

	GetCharacterMovement()->JumpZVelocity = 700.f;
	GetCharacterMovement()->AirControl = 0.35f;
	GetCharacterMovement()->MaxWalkSpeed = 500.f;
	GetCharacterMovement()->MinAnalogWalkSpeed = 20.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 2000.f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	//CameraBoom->TargetArmLength = 400.0f; // The camera follows at this distance behind the character
	//CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation

	WeaponComponent = CreateDefaultSubobject<UDjabloWeaponComponent>("WeaponComponent");
}

void ADjabloPlayerCharacter::BeginPlay()
{
	// Call the base class
	Super::BeginPlay();

	//Add Input Mapping Context
	if (APlayerController* PlayerController = Cast<APlayerController>(Controller)) {
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer())) {
			Subsystem->AddMappingContext(DefaultMappingContext, 0);
		}
	}
}

void ADjabloPlayerCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(PlayerInputComponent)) {
		EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &ADjabloPlayerCharacter::Move);

		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Started, WeaponComponent, &UDjabloWeaponComponent::StartFire);
		EnhancedInputComponent->BindAction(FireAction, ETriggerEvent::Completed, WeaponComponent, &UDjabloWeaponComponent::StopFire);

		EnhancedInputComponent->BindAction(ReloadAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::Reload);
		EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::Interact);
		EnhancedInputComponent->BindAction(PauseAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::Pause);

		EnhancedInputComponent->BindAction(PistolAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::SetWeaponPistol);
		EnhancedInputComponent->BindAction(RifleAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::SetWeaponRifle);
		EnhancedInputComponent->BindAction(ShotgunAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::SetWeaponShotgun);
		EnhancedInputComponent->BindAction(GrenadeLauncherAction, ETriggerEvent::Started, this, &ADjabloPlayerCharacter::SetWeaponGrenadeLauncher);
	}
}

void ADjabloPlayerCharacter::Reload(const FInputActionValue& Value)
{
	WeaponComponent->Reload();
}

void ADjabloPlayerCharacter::Pause(const FInputActionValue& Value)
{
	const auto World = GetWorld();
	if (!World) {
		return;
	}

	const auto GameMode = World->GetAuthGameMode();
	if (!GameMode) {
		return;
	}

	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) {
		return;
	}

	GameMode->SetPause(PlayerController);
}

void ADjabloPlayerCharacter::Move(const FInputActionValue& Value)
{
	// input is a Vector2D
	FVector2D MovementVector = Value.Get<FVector2D>();

	if (Controller != nullptr) {
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation().Add(0.f, 45.f, 0.f);
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector ForwardDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);

		// get right vector
		const FVector RightDirection = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);

		// add movement
		AddMovementInput(ForwardDirection, MovementVector.Y);
		AddMovementInput(RightDirection, MovementVector.X);
	}
}

void ADjabloPlayerCharacter::Interact(const FInputActionValue& Value)
{
	//TArray<AActor*> Result;
	//GetOverlappingActors(Result, AShotusBasePickup::StaticClass());

	//for (auto r : Result) {
	//	Cast<AShotusBasePickup>(r)->TakePickup(this);
	//}
}

void ADjabloPlayerCharacter::SetWeaponPistol()
{
	WeaponComponent->EquipWeapon(EWeaponName::Pistol);
}

void ADjabloPlayerCharacter::SetWeaponRifle()
{
	WeaponComponent->EquipWeapon(EWeaponName::Rifle);
}

void ADjabloPlayerCharacter::SetWeaponShotgun()
{
	WeaponComponent->EquipWeapon(EWeaponName::Shotgun);
}

void ADjabloPlayerCharacter::SetWeaponGrenadeLauncher()
{
	WeaponComponent->EquipWeapon(EWeaponName::GrenadeLauncher);
}

void ADjabloPlayerCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APlayerController* PlayerController = Cast<APlayerController>(Controller);
	if (!PlayerController) {
		return;
	}

	FHitResult HitResult;
	PlayerController->GetHitResultUnderCursor(ECollisionChannel::ECC_WorldStatic, true, HitResult);
	// Debug Visualization of pointer location target considering collisions
	//DrawDebugSphere(GetWorld(), HitResult.ImpactPoint, 20.f, 12, FColor::Red, false);

	const auto ActorUnderCursor = Cast<APawn>(HitResult.GetActor());
	if (ActorUnderCursor) {
		RotateTo(ActorUnderCursor->GetActorLocation());
	} else {
		RotateTo(HitResult.ImpactPoint);
	}
}

void ADjabloPlayerCharacter::RotateTo(FVector LookAtTarget)
{
	FVector ToTarget = LookAtTarget - GetMesh()->GetComponentLocation();
	FRotator LookAtRotation = ToTarget.Rotation();
	LookAtRotation.Roll = 0.f;
	LookAtRotation.Pitch = 0.f;

	SetActorRotation(FMath::RInterpTo(
		GetActorRotation(),
		LookAtRotation,
		UGameplayStatics::GetWorldDeltaSeconds(this),
		10.f
	));
}

void ADjabloPlayerCharacter::OnDeath()
{
	Super::OnDeath();

	WeaponComponent->StopFire();

	if (Controller) {
		Controller->ChangeState(NAME_Spectating);
	}

	const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
	if (GameMode) {
		GameMode->GameOver(false);
	}
}