#include "DjabloPlayerController.h"
#include "DjabloGameMode.h"


void ADjabloPlayerController::BeginPlay()
{
	Super::BeginPlay();

	if (GetWorld()) {
		const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode) {
			GameMode->OnGameStateChanged.AddUObject(this, &ADjabloPlayerController::OnGameStateChanged);
		}
	}
}

// https://superyateam.com/2019/10/01/problem-with-set-input-mode-game-only-node/
void ADjabloPlayerController::OnGameStateChanged(EGameState GameState)
{
	if (GameState == InProgress) {
		SetInputModeGameOnly(false);
	}
	else {
		SetInputMode(FInputModeUIOnly());
	}
	
}

void ADjabloPlayerController::SetInputModeGameOnly(bool InConsumeCaptureMouseDown)
{
	FInputModeGameOnly InputMode;
	InputMode.SetConsumeCaptureMouseDown(InConsumeCaptureMouseDown);
	SetInputMode(InputMode);
}
