#include "DjabloGameMode.h"
#include "DjabloEnemyAICharacter.h"
#include "DjabloCoreTypes.h"
#include <Kismet/GameplayStatics.h>


bool ADjabloGameMode::SetPause(APlayerController* PC, FCanUnpause CanUnpauseDelegate)
{
	const auto PauseSet = Super::SetPause(PC, CanUnpauseDelegate);

	if (PauseSet) {
		GameState = EGameState::Paused;
		OnGameStateChanged.Broadcast(GameState);
	}

	return PauseSet;
}

bool ADjabloGameMode::ClearPause()
{
	const auto PauseCleared = Super::ClearPause();
	if (PauseCleared) {
		GameState = EGameState::InProgress;
		OnGameStateChanged.Broadcast(GameState);
	}

	return PauseCleared;
}

void ADjabloGameMode::IncrementKillsNum()
{
	KillsNum++;

	if (KillsNum >= TotalEnemiesOnLevel) {
		OnLevelCleared.Broadcast();
		GameOver(true);
	}
}

void ADjabloGameMode::GameOver(bool IsWon)
{
	GameState = IsWon ? EGameState::GameOverWon : EGameState::GameOverLost;

	OnGameStateChanged.Broadcast(GameState);
}

void ADjabloGameMode::BeginPlay()
{
	TArray<AActor*> FoundActors;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ADjabloEnemyAICharacter::StaticClass(), FoundActors);

	TotalEnemiesOnLevel = FoundActors.Num();
}

void ADjabloGameMode::StartPlay()
{
	Super::StartPlay();

	GameState = EGameState::InProgress;

	OnGameStateChanged.Broadcast(GameState);
}
