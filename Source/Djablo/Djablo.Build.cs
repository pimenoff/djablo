using UnrealBuildTool;

public class Djablo : ModuleRules
{
	public Djablo(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] {
            "Core",
            "CoreUObject",
            "Engine",
            "InputCore",
            "EnhancedInput",
            "Niagara",
            "PhysicsCore",
            "GameplayTasks",
            "NavigationSystem",
            "UMG",
        });

        PrivateDependencyModuleNames.AddRange(new string[] {
            "GameplayTags",
			"GameplayAbilities",
        });

        PublicIncludePaths.AddRange(new string[] {
            "Djablo",
            "Djablo/Components",
            "Djablo/Weapon",
            "Djablo/Weapon/Components",
            //"Djablo/UI",
            //"Djablo/Pickups",
            //"Djablo/Zones",
            "Djablo/Player",
            "Djablo/AI",
            "Djablo/AI/Walker",
            "Djablo/AI/Thrower",
            "Djablo/AI/Turret",
            "Djablo/AI/Drone",
            "Djablo/AI/Tasks",
            "Djablo/AI/Decorators",
            "Djablo/AI/Services"
        });
    }
}
