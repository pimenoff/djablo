#include "DjabloAIControllerBase.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <DjabloEnemyAIController.h>
#include <DjabloEnemyAICharacter.h>
#include "DjabloEnemyAIPerceptionComponent.h"


ADjabloAIControllerBase::ADjabloAIControllerBase()
{
    AIPerceptionComponent = CreateDefaultSubobject<UDjabloEnemyAIPerceptionComponent>("AIPerceptionComponent");
    SetPerceptionComponent(*AIPerceptionComponent);

    SetGenericTeamId(FGenericTeamId(TeamNumber));
    //UAIPerceptionSystem::GetCurrent(GetWorld())->UpdateListener(*PerceptionComponent);
}

void ADjabloAIControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if (const auto DjabloEnemyAICharacter = Cast<ADjabloEnemyAICharacter>(InPawn)) {
        RunBehaviorTree(DjabloEnemyAICharacter->BehaviorTree);
    }
}

AActor* ADjabloAIControllerBase::GetFocusOnActor() const
{
    const auto BlackboardComponent = GetBlackboardComponent();

    if (!BlackboardComponent) {
        return nullptr;
    }

    return Cast<AActor>(BlackboardComponent->GetValueAsObject(FocusOnKeyName));
}

void ADjabloAIControllerBase::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
    SetFocus(GetFocusActor());
}

ETeamAttitude::Type ADjabloAIControllerBase::GetTeamAttitudeTowards(const AActor& Other) const
{
    if (const APawn* OtherPawn = Cast<APawn>(&Other)) {
        if (const IGenericTeamAgentInterface* TeamAgent = Cast<IGenericTeamAgentInterface>(OtherPawn->GetController())) {
            return Super::GetTeamAttitudeTowards(*OtherPawn->GetController());
        }
    }

    return ETeamAttitude::Neutral;
}
