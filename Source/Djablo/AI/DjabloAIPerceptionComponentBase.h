#pragma once

#include <CoreMinimal.h>
#include <Perception/AIPerceptionComponent.h>
#include "DjabloAIPerceptionComponentBase.generated.h"


UCLASS()
class DJABLO_API UDjabloAIPerceptionComponentBase : public UAIPerceptionComponent
{
	GENERATED_BODY()

public:
	AActor* GetClosestEnemy() const;
};
