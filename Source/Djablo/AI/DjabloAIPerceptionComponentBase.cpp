#include "DjabloAIPerceptionComponentBase.h"
#include "DjabloEnemyAIController.h"
#include "DjabloHealthComponent.h"
#include "Perception/AISense_Sight.h"
#include "Perception/AISense_Damage.h"


AActor* UDjabloAIPerceptionComponentBase::GetClosestEnemy() const
{
    TArray<AActor*> PercieveActors;

    GetCurrentlyPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
    if (PercieveActors.Num() == 0) {
        GetKnownPerceivedActors(UAISense_Sight::StaticClass(), PercieveActors);
        if (PercieveActors.Num() == 0) {
            GetPerceivedActors(UAISense_Damage::StaticClass(), PercieveActors);
            if (PercieveActors.Num() == 0) {
                return nullptr;
            }
		}
    }

    const auto Controller = Cast<AAIController>(GetOwner());
    if (!Controller) {
        return nullptr;
    }

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) {
        return nullptr;
    }

    float BestDistance = MAX_FLT;
    AActor* BestPawn = nullptr;
    for (const auto PercieveActor : PercieveActors) {
        const auto HealthComponent = PercieveActor->FindComponentByClass<UDjabloHealthComponent>();
        const auto PercievePawn = Cast<APawn>(PercieveActor);

        if (HealthComponent && !HealthComponent->IsDead()) {
            const auto CurrentDistance = (PercieveActor->GetActorLocation() - Pawn->GetActorLocation()).Size();

            if (CurrentDistance < BestDistance) {
                BestDistance = CurrentDistance;
                BestPawn = PercieveActor;
            }
        }
    }

    return BestPawn;
}