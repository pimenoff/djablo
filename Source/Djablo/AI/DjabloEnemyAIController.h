#pragma once

#include <CoreMinimal.h>
#include "DjabloAIControllerBase.h"
#include "DjabloEnemyAIController.generated.h"


UCLASS()
class DJABLO_API ADjabloEnemyAIController : public ADjabloAIControllerBase
{
	GENERATED_BODY()

public:
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;
};
