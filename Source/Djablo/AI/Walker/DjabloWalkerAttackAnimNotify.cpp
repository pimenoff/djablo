#include "DjabloWalkerAttackAnimNotify.h"
#include "DjabloWalkerAICharacter.h"


void UDjabloWalkerAttackAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Walker = Cast<ADjabloWalkerAICharacter>(MeshComp->GetOwner());
	if (Walker) {
		Walker->Attack();
		Walker->bIsAttacking = false;
	}
}
