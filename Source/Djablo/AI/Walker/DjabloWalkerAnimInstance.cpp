#include "DjabloWalkerAnimInstance.h"
#include "DjabloWalkerAICharacter.h"
#include <GameFramework/CharacterMovementComponent.h>


void UDjabloWalkerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto Walker = Cast<ADjabloWalkerAICharacter>(TryGetPawnOwner());
	if (Walker && Walker->IsValidLowLevel()) {
		Speed = Walker->GetCharacterMovement()->Velocity.Size();
		IdleState = Walker->IdleState;
		bIsAttacking = Walker->bIsAttacking;
	}
}
