#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimNotifies/AnimNotify.h>
#include "DjabloWalkerStandingAnimNotify.generated.h"


UCLASS()
class DJABLO_API UDjabloWalkerStandingAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
