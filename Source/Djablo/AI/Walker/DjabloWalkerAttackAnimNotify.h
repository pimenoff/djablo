#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimNotifies/AnimNotify.h>
#include "DjabloWalkerAttackAnimNotify.generated.h"


UCLASS()
class DJABLO_API UDjabloWalkerAttackAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
