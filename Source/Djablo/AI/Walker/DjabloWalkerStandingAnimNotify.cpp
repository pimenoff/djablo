#include "DjabloWalkerStandingAnimNotify.h"
#include "DjabloWalkerAnimInstance.h"
#include "DjabloWalkerAICharacter.h"


void UDjabloWalkerStandingAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Walker = Cast<ADjabloWalkerAICharacter>(MeshComp->GetOwner());
	auto AnimInstance = Cast<UDjabloWalkerAnimInstance>(MeshComp->GetAnimInstance());

	if (Walker && AnimInstance) {
		Walker->IdleState = EWalkerIdleState::Standup;
		AnimInstance->IdleState = EWalkerIdleState::Standup;
	}
}
