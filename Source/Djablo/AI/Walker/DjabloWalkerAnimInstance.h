#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimInstance.h>
#include "DjabloCoreTypes.h"
#include "DjabloWalkerAnimInstance.generated.h"


UCLASS()
class DJABLO_API UDjabloWalkerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()
	
public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	float Speed;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EWalkerIdleState> IdleState;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	bool bIsAttacking;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;

};
