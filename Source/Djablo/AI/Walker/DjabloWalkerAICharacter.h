#pragma once

#include <CoreMinimal.h>
#include <DjabloEnemyAICharacter.h>
#include "DjabloCoreTypes.h"
#include "DjabloWalkerAICharacter.generated.h"


class UBillboardComponent;

UCLASS()
class DJABLO_API ADjabloWalkerAICharacter : public ADjabloEnemyAICharacter
{
	GENERATED_BODY()

public:
	ADjabloWalkerAICharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EWalkerIdleState> IdleState;

	UPROPERTY(EditAnywhere, Category = "AI")
	UBillboardComponent* CrawlTarget;

	UPROPERTY(EditAnywhere, Category = "Weapon")
	float MeleeAttackDamage = 100.f;

	bool Attack() override;

	bool bIsAttacking = false;

protected:

	virtual void BeginPlay() override;
	void OnDeath() override;

private:
	FTimerHandle MontageTimerHandle;

};
