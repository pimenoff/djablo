#include "DjabloWalkerAICharacter.h"
#include "DjabloEnemyAIController.h"
#include <Components/BillboardComponent.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <BehaviorTree/BehaviorTree.h>
#include <BehaviorTree/BlackboardComponent.h>
#include "DjabloPlayerCharacter.h"


ADjabloWalkerAICharacter::ADjabloWalkerAICharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
    CrawlTarget = CreateDefaultSubobject<UBillboardComponent>(TEXT("CrawlTarget"));
    CrawlTarget->SetupAttachment(RootComponent);
}

bool ADjabloWalkerAICharacter::Attack()
{
    return MakeMeleeAttack(MeleeAttackDamage);
}

void ADjabloWalkerAICharacter::BeginPlay()
{
    Super::BeginPlay();

    const auto AIController = Cast<ADjabloEnemyAIController>(GetController());
    if (AIController && CrawlTarget) {
        const auto Blackboard = AIController->GetBlackboardComponent();
        if (Blackboard) {
            Blackboard->SetValueAsVector(FName("CrawlTarget"), CrawlTarget->GetComponentTransform().GetLocation());
        }
    }
}

void ADjabloWalkerAICharacter::OnDeath()
{
    Super::OnDeath();

    IdleState = EWalkerIdleState::CrawlingAmbush;
}
