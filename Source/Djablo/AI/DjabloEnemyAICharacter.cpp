#include "DjabloEnemyAICharacter.h"
#include "DjabloEnemyAIController.h"
#include "DjabloPlayerCharacter.h"
#include "DjabloGameMode.h"
#include <BrainComponent.h>
#include <Kismet/KismetSystemLibrary.h>
#include <Kismet/GameplayStatics.h>
#include <GameFramework/CharacterMovementComponent.h>


ADjabloEnemyAICharacter::ADjabloEnemyAICharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
    AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
    AIControllerClass = ADjabloEnemyAIController::StaticClass();

    bUseControllerRotationYaw = false;
    if (GetCharacterMovement()) {
        GetCharacterMovement()->bUseControllerDesiredRotation = true;
        GetCharacterMovement()->RotationRate = FRotator(0.0f, 200.0f, 0.0f);
    }
}


bool ADjabloEnemyAICharacter::MakeMeleeAttack(float MeleeDamage, float MeleeSphereRadius)
{
    auto SkeletalMesh = GetMesh();
    if (!SkeletalMesh) {
        return false;
    }

    auto BonePos = SkeletalMesh->GetSocketLocation("RightHand");

    TArray<TEnumAsByte<EObjectTypeQuery>> TraceObjectTypes;
    TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECollisionChannel::ECC_Pawn));

    TArray<AActor*> IgnoreActors;
    IgnoreActors.Init(this, 1);

    TArray<AActor*> OutActors;

    UKismetSystemLibrary::SphereOverlapActors(
        GetWorld(),
        BonePos,
        MeleeSphereRadius,
        TraceObjectTypes,
        ADjabloPlayerCharacter::StaticClass(),
        IgnoreActors,
        OutActors
    );

    for (auto& Actor : OutActors) {
        auto Player = Cast<ADjabloPlayerCharacter>(Actor);
        if (Player) {
            UGameplayStatics::ApplyDamage(
                Player,
                MeleeDamage,
                GetController(),
                this,
                UDamageType::StaticClass()
            );
        }
    }

    return true;
}

void ADjabloEnemyAICharacter::OnDeath()
{
    Super::OnDeath();

    const auto AIController = Cast<AAIController>(Controller);
    if (AIController && AIController->BrainComponent)
    {
        AIController->BrainComponent->Cleanup();
    }
}

void ADjabloEnemyAICharacter::FinalizeDeath()
{
    const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
    if (GameMode) {
        GameMode->IncrementKillsNum();
    }
}
