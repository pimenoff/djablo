#pragma once

#include <CoreMinimal.h>
#include "DjabloCharacterBase.h"
#include "DjabloEnemyAICharacter.generated.h"


class UBehaviorTree;
class UAnimMontage;


UCLASS()
class DJABLO_API ADjabloEnemyAICharacter : public ADjabloCharacterBase
{
	GENERATED_BODY()

public:
	ADjabloEnemyAICharacter(const FObjectInitializer& ObjInit);

	virtual bool Attack() { return true; };

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* BehaviorTree;

protected:
	bool MakeMeleeAttack(float MeleeDamage, float MeleeSphereRadius = 50.f);

	void OnDeath() override;
	virtual void FinalizeDeath() override;
};
