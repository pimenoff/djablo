#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/BTDecorator.h>
#include "DjabloCoreTypes.h"
#include "DjabloThrowerIdleStateDecorator.generated.h"


UCLASS()
class DJABLO_API UDjabloThrowerIdleStateDecorator : public UBTDecorator
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EThrowerIdleState> CheckIdleState;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
