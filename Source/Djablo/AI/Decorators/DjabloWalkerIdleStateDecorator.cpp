#include "DjabloWalkerIdleStateDecorator.h"
#include "DjabloWalkerAICharacter.h"
#include "DjabloCoreTypes.h"
#include <AIController.h>
#include <BehaviorTree/BTDecorator.h>


bool UDjabloWalkerIdleStateDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	//bool bSuccess = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);
	//if (!bSuccess) {
	//	return false;
	//}

	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) {
		return false;
	}

	const auto Thrower = Cast<ADjabloWalkerAICharacter>(Controller->GetCharacter());
	if (!Thrower) {
		return false;
	}

	return Thrower->IdleState == CheckIdleState;
}
