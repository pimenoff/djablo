#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/BTDecorator.h>
#include "DjabloCoreTypes.h"
#include "DjabloWalkerIdleStateDecorator.generated.h"


UCLASS()
class DJABLO_API UDjabloWalkerIdleStateDecorator : public UBTDecorator
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EWalkerIdleState> CheckIdleState;

	virtual bool CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const override;
};
