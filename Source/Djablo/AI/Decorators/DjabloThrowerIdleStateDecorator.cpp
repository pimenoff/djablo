#include "DjabloThrowerIdleStateDecorator.h"
#include "DjabloThrowerAICharacter.h"
#include "DjabloCoreTypes.h"
#include <AIController.h>
#include <BehaviorTree/BTDecorator.h>


bool UDjabloThrowerIdleStateDecorator::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	//bool bSuccess = Super::CalculateRawConditionValue(OwnerComp, NodeMemory);
	//if (!bSuccess) {
	//	return false;
	//}

	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) {
		return false;
	}

	const auto Thrower = Cast<ADjabloThrowerAICharacter>(Controller->GetCharacter());
	if (!Thrower) {
		return false;
	}

	return Thrower->IdleState == CheckIdleState;
}
