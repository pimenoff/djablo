#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Pawn.h>
#include "DjabloDroneCharacter.generated.h"


class UBehaviorTree;

UCLASS()
class DJABLO_API ADjabloDroneCharacter : public APawn
{
	GENERATED_BODY()

public:
	ADjabloDroneCharacter();

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* BehaviorTree;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
