#include "DjabloDroneCharacter.h"
#include "DjabloDroneAIController.h"


ADjabloDroneCharacter::ADjabloDroneCharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ADjabloDroneAIController::StaticClass();
}

void ADjabloDroneCharacter::BeginPlay()
{
	Super::BeginPlay();	
}

void ADjabloDroneCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void ADjabloDroneCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

