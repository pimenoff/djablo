#include "DjabloDroneAIController.h"
#include "DjabloPlayerCharacter.h"
#include "DjabloDroneCharacter.h"


ETeamAttitude::Type ADjabloDroneAIController::GetTeamAttitudeTowards(const AActor& Other) const
{
    if (const auto OtherPawn = Cast<ADjabloPlayerCharacter>(&Other)) {
        return ETeamAttitude::Friendly;
    }

    return ETeamAttitude::Neutral;
}

void ADjabloDroneAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if (const auto DroneCharacter = Cast<ADjabloDroneCharacter>(InPawn)) {
        RunBehaviorTree(DroneCharacter->BehaviorTree);
    }
}