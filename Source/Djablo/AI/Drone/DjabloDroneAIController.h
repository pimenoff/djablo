#pragma once

#include <CoreMinimal.h>
#include "DjabloAIControllerBase.h"
#include "DjabloDroneAIController.generated.h"


UCLASS()
class DJABLO_API ADjabloDroneAIController : public ADjabloAIControllerBase
{
	GENERATED_BODY()

public:
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

protected:
	virtual void OnPossess(APawn* InPawn) override;
};
