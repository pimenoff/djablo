#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "DjabloFindPlayerService.generated.h"


UCLASS()
class DJABLO_API UDjabloFindPlayerService : public UBTService
{
	GENERATED_BODY()

public:
	UDjabloFindPlayerService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector PlayerActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
