#include "DjabloFindPlayerService.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <GameFramework/Character.h>
#include <AIController.h>


UDjabloFindPlayerService::UDjabloFindPlayerService()
{
	NodeName = "Find player";
}

void UDjabloFindPlayerService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto World = GetWorld();
	const auto Controller = OwnerComp.GetAIOwner();
	const auto Blackboard = OwnerComp.GetBlackboardComponent();

	if (World && Controller && Blackboard) {
		const auto PlayerController = World->GetFirstPlayerController();
		if (PlayerController) {
			auto Character = PlayerController->GetCharacter();
			if (Character) {
				Blackboard->SetValueAsObject(PlayerActorKey.SelectedKeyName, Character);
			}
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
