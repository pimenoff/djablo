#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "DjabloFindEnemyService.generated.h"


UCLASS()
class DJABLO_API UDjabloFindEnemyService : public UBTService
{
	GENERATED_BODY()

public:
	UDjabloFindEnemyService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyActorKey;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
