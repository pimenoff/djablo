#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/BTService.h>
#include "DjabloCoreTypes.h"
#include "DjabloWaitForStandUpService.generated.h"


UCLASS()
class DJABLO_API UDjabloWaitForStandUpService : public UBTService
{
	GENERATED_BODY()

public:
	UDjabloWaitForStandUpService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector HasInWalkerIdleStateKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EWalkerIdleState> ExpectedIdleState;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
