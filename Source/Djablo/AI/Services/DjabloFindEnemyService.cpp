#include "DjabloFindEnemyService.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <AIController.h>
#include "DjabloEnemyAIPerceptionComponent.h"


UDjabloFindEnemyService::UDjabloFindEnemyService()
{
	NodeName = "Find enemy";
}

void UDjabloFindEnemyService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();

	if (Controller) {
		const auto Blackboard = OwnerComp.GetBlackboardComponent();
		const auto PerceptionComponent = Controller->FindComponentByClass<UDjabloEnemyAIPerceptionComponent>();

		if (PerceptionComponent) {
			Blackboard->SetValueAsObject(EnemyActorKey.SelectedKeyName, PerceptionComponent->GetClosestEnemy());
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}
