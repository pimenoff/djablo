#include "DjabloWaitForStandUpService.h"
#include "DjabloWalkerAICharacter.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <AIController.h>
#include <GameFramework/Character.h>


UDjabloWaitForStandUpService::UDjabloWaitForStandUpService()
{
	NodeName = "Sets standup key";
}

void UDjabloWaitForStandUpService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();

	if (Controller) {
		const auto Character = Cast<ADjabloWalkerAICharacter>(Controller->GetCharacter());
		const auto Blackboard = OwnerComp.GetBlackboardComponent();
		if (Character && Blackboard) {
			Blackboard->SetValueAsBool(HasInWalkerIdleStateKey.SelectedKeyName, Character->IdleState == ExpectedIdleState);
		}
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}