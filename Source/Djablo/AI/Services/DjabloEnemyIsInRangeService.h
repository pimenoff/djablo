#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "DjabloEnemyIsInRangeService.generated.h"


UCLASS()
class DJABLO_API UDjabloEnemyIsInRangeService : public UBTService
{
	GENERATED_BODY()

public:
	UDjabloEnemyIsInRangeService();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector EnemyActorKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector IsInRangeKey;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float Radius = 100.0f;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
