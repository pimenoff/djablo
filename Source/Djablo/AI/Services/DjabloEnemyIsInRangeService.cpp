#include "DjabloEnemyIsInRangeService.h"
#include "DjabloEnemyAIPerceptionComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"


UDjabloEnemyIsInRangeService::UDjabloEnemyIsInRangeService()
{
	NodeName = "Check if enemy in attack range";
}

void UDjabloEnemyIsInRangeService::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	const auto Controller = OwnerComp.GetAIOwner();

	if (Controller) {
		const auto Blackboard = OwnerComp.GetBlackboardComponent();
		const AActor* EnemyActor = Cast<AActor>(Blackboard->GetValueAsObject(EnemyActorKey.SelectedKeyName));
		Blackboard->SetValueAsBool(IsInRangeKey.SelectedKeyName, EnemyActor && (EnemyActor->GetDistanceTo(Controller->GetPawn()) <= Radius));
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("Controller null"));
	}

	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);
}