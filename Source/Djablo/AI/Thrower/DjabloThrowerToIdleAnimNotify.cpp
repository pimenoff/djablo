#include "DjabloThrowerToIdleAnimNotify.h"
#include "DjabloThrowerAICharacter.h"
#include "DjabloCoreTypes.h"


void UDjabloThrowerToIdleAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Thrower = Cast<ADjabloThrowerAICharacter>(MeshComp->GetOwner());
	if (Thrower) {
		Thrower->IdleState = EThrowerIdleState::Idle;
	}
}
