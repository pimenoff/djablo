#include "DjabloThrowerThrowAnimNotify.h"
#include "DjabloThrowerAICharacter.h"


void UDjabloThrowerThrowAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Thrower = Cast<ADjabloThrowerAICharacter>(MeshComp->GetOwner());
	if (Thrower) {
		Thrower->Throw();
	}
}
