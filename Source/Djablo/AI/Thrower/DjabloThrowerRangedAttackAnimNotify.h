#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimNotifies/AnimNotify.h>
#include "DjabloThrowerRangedAttackAnimNotify.generated.h"


UCLASS()
class DJABLO_API UDjabloThrowerRangedAttackAnimNotify : public UAnimNotify
{
	GENERATED_BODY()

public:
	virtual void Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation);
};
