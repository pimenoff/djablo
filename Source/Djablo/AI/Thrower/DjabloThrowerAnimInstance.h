#pragma once

#include <CoreMinimal.h>
#include <Animation/AnimInstance.h>
#include "DjabloCoreTypes.h"
#include "DjabloThrowerAnimInstance.generated.h"


UCLASS()
class DJABLO_API UDjabloThrowerAnimInstance : public UAnimInstance
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
	TEnumAsByte<EThrowerIdleState> IdleState;

	virtual void NativeUpdateAnimation(float DeltaSeconds) override;
};
