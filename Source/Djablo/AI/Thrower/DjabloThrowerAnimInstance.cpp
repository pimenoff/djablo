#include "DjabloThrowerAnimInstance.h"
#include "DjabloThrowerAICharacter.h"


void UDjabloThrowerAnimInstance::NativeUpdateAnimation(float DeltaSeconds)
{
	Super::NativeUpdateAnimation(DeltaSeconds);

	auto Thrower = Cast<ADjabloThrowerAICharacter>(TryGetPawnOwner());
	if (Thrower && Thrower->IsValidLowLevel()) {
		IdleState = Thrower->IdleState;
	}
}
