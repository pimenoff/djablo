#include "DjabloThrowerMeleeAnimNotify.h"
#include "DjabloThrowerAICharacter.h"


void UDjabloThrowerMeleeAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Thrower = Cast<ADjabloThrowerAICharacter>(MeshComp->GetOwner());
	if (Thrower) {
		Thrower->Attack();
	}
}
