#include "DjabloThrowerRangedAttackAnimNotify.h"
#include "DjabloThrowerAICharacter.h"


void UDjabloThrowerRangedAttackAnimNotify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	Super::Notify(MeshComp, Animation);

	auto Thrower = Cast<ADjabloThrowerAICharacter>(MeshComp->GetOwner());
	if (Thrower) {
		Thrower->RangedAttack();
	}
}
