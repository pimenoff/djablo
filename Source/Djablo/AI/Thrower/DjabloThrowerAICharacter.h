#pragma once

#include <CoreMinimal.h>
#include "DjabloEnemyAICharacter.h"
#include "DjabloCoreTypes.h"
#include "DjabloThrowerAICharacter.generated.h"


class ADjabloProjectile;
class USkeletalMeshComponent;
class USoundCue;


UCLASS()
class DJABLO_API ADjabloThrowerAICharacter : public ADjabloEnemyAICharacter
{
	GENERATED_BODY()

public:
	ADjabloThrowerAICharacter(const FObjectInitializer& ObjInit);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EThrowerIdleState> IdleState;

	UFUNCTION(BlueprintCallable, Category = "AI")
	bool Attack() override;

	UFUNCTION(BlueprintCallable, Category = "AI")
	void Throw();

	UFUNCTION(BlueprintCallable, Category = "AI")
	void RangedAttack();

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	USkeletalMeshComponent* BackWeaponMesh1;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ADjabloProjectile> ProjectileClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	TSubclassOf<ADjabloProjectile> BackWeaponProjectileClass;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName BackWeaponSocketName = "SpineLauncherSocket";

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName BackWeaponMeshProjectileSpawnSocketName = "ProjectileSpawnSocket";

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Weapon")
	FDecalData BackWeaponHighlightDecalData;

	UPROPERTY(EditAnywhere, Category = "Weapon")
	float MeleeAttackDamage = 100.f;

	UPROPERTY(EditAnywhere, Category = "Weapon")
	int RangedAttackProjectileCount = 5;

	UPROPERTY(EditAnywhere, Category = "Weapon")
	FRingInt RangedAttackTargetSpread;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* RangedAttackSound;

	virtual void BeginPlay() override;

private:
	FTimerHandle RangedAttackTimerHandle;
	int CurrentRangedAttackCount;
	void RangedAttackFire();
};
