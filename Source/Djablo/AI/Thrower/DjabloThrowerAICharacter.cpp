#include "DjabloThrowerAICharacter.h"
#include "DjabloProjectile.h"
#include "DjabloCoreTypes.h"
#include <Sound/SoundCue.h>
#include <Components/SkeletalMeshComponent.h>
#include <Components/CapsuleComponent.h>
#include <GameFramework/ProjectileMovementComponent.h>
#include <GameFramework/CharacterMovementComponent.h>
#include <GameFramework/Character.h>
#include <Kismet/GameplayStatics.h>
#include <Kismet/GameplayStaticsTypes.h>


ADjabloThrowerAICharacter::ADjabloThrowerAICharacter(const FObjectInitializer& ObjInit) : Super(ObjInit)
{
	BackWeaponMesh1 = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("BackWeapon1"));
	BackWeaponMesh1->AttachToComponent(
		GetMesh(),
		FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName(BackWeaponSocketName)
	);

	IdleState = EThrowerIdleState::Idle;
}

void ADjabloThrowerAICharacter::BeginPlay()
{
	Super::BeginPlay();

	const auto Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	//FFindFloorResult FindFloorResult;
	//Player->GetCharacterMovement()->FindFloor(
	//	Player->GetCapsuleComponent()->GetComponentLocation(),
	//	FindFloorResult,
	//	false
	//);

	//DrawDebugSphere(GetWorld(), FindFloorResult.HitResult.ImpactPoint, 10, 24, FColor::Red, false, 60.0f);
	//auto Decal = UGameplayStatics::SpawnDecalAtLocation(
	//	GetWorld(),
	//	BackWeaponHighlightDecalData.Material,
	//	BackWeaponHighlightDecalData.Size,
	//	FindFloorResult.HitResult.ImpactPoint
	//	//FRotator(90, 90, 90)
	//);
}


bool ADjabloThrowerAICharacter::Attack()
{
	return MakeMeleeAttack(MeleeAttackDamage);
}

void ADjabloThrowerAICharacter::Throw()
{
	FVector OutLaunchVelocity;
	const FVector StartLocation = GetMesh()->GetSocketLocation("RightHand");
	const auto Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);

	const FTransform SpawnTransform(FRotator::ZeroRotator, StartLocation);
	auto Projectile = GetWorld()->SpawnActorDeferred<ADjabloProjectile>(ProjectileClass, SpawnTransform);

	if (Projectile && UGameplayStatics::SuggestProjectileVelocity(
		this,
		OutLaunchVelocity,
		StartLocation,
		Player->GetActorLocation(),
		Projectile->ProjectileMovementComponent->InitialSpeed,
		false,
		0.f,
		0.f,
		ESuggestProjVelocityTraceOption::DoNotTrace,
		FCollisionResponseParams::DefaultResponseParam,
		TArray<AActor*>(),
		false

	)) {
		Projectile->SetShotDirection(OutLaunchVelocity.GetSafeNormal());
		Projectile->SetOwner(this);
		Projectile->FinishSpawning(SpawnTransform);
	}
}

void ADjabloThrowerAICharacter::RangedAttack()
{
	CurrentRangedAttackCount = RangedAttackProjectileCount;

	if (RangedAttackSound) {
		UGameplayStatics::PlaySound2D(GetWorld(), RangedAttackSound);
	}

	GetWorld()->GetTimerManager().SetTimer(
		RangedAttackTimerHandle,
		this,
		&ADjabloThrowerAICharacter::RangedAttackFire,
		0.2f,
		true
	);
}

void ADjabloThrowerAICharacter::RangedAttackFire()
{
	FVector OutLaunchVelocity;
	const FVector StartLocation = BackWeaponMesh1->GetSocketLocation(BackWeaponMeshProjectileSpawnSocketName);
	const auto Player = UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	const FTransform SpawnTransform(FRotator::ZeroRotator, StartLocation);

	auto Projectile = GetWorld()->SpawnActorDeferred<ADjabloProjectile>(
		BackWeaponProjectileClass,
		SpawnTransform
	);

	if (Projectile) {
		auto RandPoint = FMath::VRand();
		RandPoint *= FMath::RandRange(RangedAttackTargetSpread.InnerRadius, RangedAttackTargetSpread.OuterRadius);

		FFindFloorResult FindFloorResult;
		Player->GetCharacterMovement()->FindFloor(
			Player->GetCapsuleComponent()->GetComponentLocation(),
			FindFloorResult,
			true
		);

		auto ShotLocation = FindFloorResult.HitResult.ImpactPoint;
		ShotLocation.X += RandPoint.X;
		ShotLocation.Y += RandPoint.Y;

		if (UGameplayStatics::SuggestProjectileVelocity(
			this,
			OutLaunchVelocity,
			StartLocation,
			ShotLocation,
			Projectile->ProjectileMovementComponent->InitialSpeed,
			true,
			0.f,
			0.f,
			ESuggestProjVelocityTraceOption::DoNotTrace,
			FCollisionResponseParams::DefaultResponseParam,
			TArray<AActor*>(),
			false
		)) {
			if (BackWeaponHighlightDecalData.Material) {
				auto Decal = UGameplayStatics::SpawnDecalAtLocation(
					GetWorld(),
					BackWeaponHighlightDecalData.Material,
					BackWeaponHighlightDecalData.Size,
					ShotLocation
				);

				if (Decal) {
					Decal->SetFadeOut(
						BackWeaponHighlightDecalData.LifeTime,
						BackWeaponHighlightDecalData.FadeOutTime
					);
				}
			}

			Projectile->SetShotDirection(OutLaunchVelocity.GetSafeNormal());
			Projectile->SetOwner(this);
			Projectile->FinishSpawning(SpawnTransform);
		}
	}

	CurrentRangedAttackCount--;
	if (!CurrentRangedAttackCount) {
		GetWorld()->GetTimerManager().ClearTimer(RangedAttackTimerHandle);
	}
}
