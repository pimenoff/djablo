#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "DjabloStandUpTask.generated.h"


UCLASS()
class DJABLO_API UDjabloStandUpTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UDjabloStandUpTask();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
