#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/BTTaskNode.h>
#include "DjabloSetlBlackboardKey.generated.h"


UCLASS()
class DJABLO_API UDjabloSetlBlackboardKey : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UDjabloSetlBlackboardKey();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FBlackboardKeySelector Key;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	bool Value;
};
