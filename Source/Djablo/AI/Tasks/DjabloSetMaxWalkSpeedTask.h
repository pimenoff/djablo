#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "DjabloSetMaxWalkSpeedTask.generated.h"


UCLASS()
class DJABLO_API UDjabloSetMaxWalkSpeedTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UDjabloSetMaxWalkSpeedTask();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	float MaxWalkSpeed = 100.0f;
};
