#include "AI/Tasks/DjabloSetlBlackboardKey.h"
#include "BehaviorTree/BlackboardComponent.h"


UDjabloSetlBlackboardKey::UDjabloSetlBlackboardKey()
{
    NodeName = "Set/unset bool value";
}

EBTNodeResult::Type UDjabloSetlBlackboardKey::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    if (!Blackboard) {
        return EBTNodeResult::Failed;
    }

    Blackboard->SetValueAsBool(Key.SelectedKeyName, Value);

    return EBTNodeResult::Succeeded;
}