#pragma once

#include <CoreMinimal.h>
#include <BehaviorTree/BTTaskNode.h>
#include "DjabloCoreTypes.h"
#include "DjabloThrowerSetIdleStateTask.generated.h"


UCLASS()
class DJABLO_API UDjabloThrowerSetIdleStateTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	TEnumAsByte<EThrowerIdleState> IdleState;
};
