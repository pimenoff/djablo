#include "DjabloFindRandomLocationTask.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <AIController.h>
#include <NavigationSystem.h>


UDjabloFindRandomLocationTask::UDjabloFindRandomLocationTask()
{
    NodeName = "Next Location";
}

EBTNodeResult::Type UDjabloFindRandomLocationTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Controller = OwnerComp.GetAIOwner();
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    if (!Controller || !Blackboard) {
        return EBTNodeResult::Failed;
    }

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) {
        return EBTNodeResult::Failed;
    }

    const auto NavSys = UNavigationSystemV1::GetCurrent(Pawn);
    if (!NavSys) {
        return EBTNodeResult::Failed;
    }

    FNavLocation NavLocation;
    auto Location = Pawn->GetActorLocation();
    const auto Found = NavSys->GetRandomReachablePointInRadius(Location, Radius, NavLocation);
    if (!Found) {
        return EBTNodeResult::Failed;
    }

    Blackboard->SetValueAsVector(TargetLocationKey.SelectedKeyName, NavLocation.Location);

    return EBTNodeResult::Succeeded;
}