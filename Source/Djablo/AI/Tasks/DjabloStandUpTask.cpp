#include "DjabloStandUpTask.h"
#include "DjabloWalkerAICharacter.h"
#include "DjabloCoreTypes.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <AIController.h>


UDjabloStandUpTask::UDjabloStandUpTask()
{
    NodeName = "Make walker standing up";
}

EBTNodeResult::Type UDjabloStandUpTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Controller = OwnerComp.GetAIOwner();
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    if (!Controller || !Blackboard) {
        return EBTNodeResult::Failed;
    }

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) {
        return EBTNodeResult::Failed;
    }

    const auto Character = Cast<ADjabloWalkerAICharacter>(Pawn);
    if (!Character) {
        return EBTNodeResult::Failed;
    }

    if (Character->IdleState != EWalkerIdleState::Standup) {
        if (Character->IdleState == EWalkerIdleState::Agonyzing
            || Character->IdleState == EWalkerIdleState::Eating) {
            Character->IdleState = EWalkerIdleState::Standup;
        }
        else {
            Character->IdleState = EWalkerIdleState::StandingUp;
        }
    }

    return EBTNodeResult::Succeeded;
}