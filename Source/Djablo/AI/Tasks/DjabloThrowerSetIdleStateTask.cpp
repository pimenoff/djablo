#include "DjabloThrowerSetIdleStateTask.h"
#include <AIController.h>
#include "DjabloThrowerAICharacter.h"


EBTNodeResult::Type UDjabloThrowerSetIdleStateTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) {
		return EBTNodeResult::Failed;
	}

	const auto Thrower = Cast<ADjabloThrowerAICharacter>(Controller->GetCharacter());
	Thrower->IdleState = IdleState;

	return EBTNodeResult::Succeeded;
}
