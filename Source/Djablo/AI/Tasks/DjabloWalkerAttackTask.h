#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "DjabloWalkerAttackTask.generated.h"


UCLASS()
class DJABLO_API UDjabloWalkerAttackTask : public UBTTaskNode
{
	GENERATED_BODY()

public:
	UDjabloWalkerAttackTask();

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
