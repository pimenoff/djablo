#include "DjabloWalkerSetIdleStateTask.h"
#include <AIController.h>
#include "DjabloWalkerAICharacter.h"


EBTNodeResult::Type UDjabloWalkerSetIdleStateTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	const auto Controller = OwnerComp.GetAIOwner();
	if (!Controller) {
		return EBTNodeResult::Failed;
	}

	const auto Walker = Cast<ADjabloWalkerAICharacter>(Controller->GetCharacter());
	Walker->IdleState = IdleState;

	return EBTNodeResult::Succeeded;
}
