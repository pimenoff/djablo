#include "DjabloSetMaxWalkSpeedTask.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "AIController.h"
#include "NavigationSystem.h"
#include "GameFramework/Character.h"
#include "GameFramework/CharacterMovementComponent.h"


UDjabloSetMaxWalkSpeedTask::UDjabloSetMaxWalkSpeedTask()
{
    NodeName = "Set max walk speed";
}

EBTNodeResult::Type UDjabloSetMaxWalkSpeedTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Controller = OwnerComp.GetAIOwner();
    const auto Blackboard = OwnerComp.GetBlackboardComponent();
    if (!Controller || !Blackboard) {
        return EBTNodeResult::Failed;
    }

    const auto Character = Controller->GetCharacter();
    if (!Character) {
        return EBTNodeResult::Failed;
    }

    const auto MovementComponent = Character->GetCharacterMovement();
    if (!MovementComponent) {
        return EBTNodeResult::Failed;
    }
    
    MovementComponent->MaxWalkSpeed = MaxWalkSpeed;

    return EBTNodeResult::Succeeded;
}