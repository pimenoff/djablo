#include "DjabloWalkerAttackTask.h"
#include <AIController.h>
#include "DjabloWalkerAICharacter.h"


UDjabloWalkerAttackTask::UDjabloWalkerAttackTask()
{
    NodeName = "Attack";
}

EBTNodeResult::Type UDjabloWalkerAttackTask::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
    const auto Controller = OwnerComp.GetAIOwner();
    if (!Controller) {
        return EBTNodeResult::Failed;
    }

    const auto Pawn = Controller->GetPawn();
    if (!Pawn) {
        return EBTNodeResult::Failed;
    }

    const auto Walker = Cast<ADjabloWalkerAICharacter>(Pawn);
    if (!Walker) {
        return EBTNodeResult::Failed;
    }

    Walker->bIsAttacking = true;

    return Super::ExecuteTask(OwnerComp, NodeMemory);
}