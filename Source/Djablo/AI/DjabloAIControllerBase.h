#pragma once

#include <CoreMinimal.h>
#include <AIController.h>
#include "DjabloAIPerceptionComponentBase.h"
#include "DjabloAIControllerBase.generated.h"


class UDjabloEnemyAIPerceptionComponent;


UCLASS()
class DJABLO_API ADjabloAIControllerBase : public AAIController
{
	GENERATED_BODY()
	
public:
	ADjabloAIControllerBase();
	virtual void Tick(float DeltaTime) override;
	ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Components")
	UDjabloAIPerceptionComponentBase* AIPerceptionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	FName FocusOnKeyName = "EnemyActor";

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	uint8 TeamNumber = 1;

	virtual void OnPossess(APawn* InPawn) override;

private:
	AActor* GetFocusOnActor() const;
};
