#include "DjabloEnemyAIController.h"
#include "DjabloDroneCharacter.h"


ETeamAttitude::Type ADjabloEnemyAIController::GetTeamAttitudeTowards(const AActor& Other) const
{
    if (const auto OtherPawn = Cast<ADjabloDroneCharacter>(&Other)) {
        return ETeamAttitude::Neutral;
    }

    return Super::GetTeamAttitudeTowards(Other);
}
