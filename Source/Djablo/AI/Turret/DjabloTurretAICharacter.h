#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Pawn.h>
#include "DjabloTurretAICharacter.generated.h"


class UDjabloHealthComponent;
class UBehaviorTree;
class USphereComponent;
class UPoseableMeshComponent;

UCLASS()
class DJABLO_API ADjabloTurretAICharacter : public APawn
{
	GENERATED_BODY()

public:
	ADjabloTurretAICharacter();

public:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	USphereComponent* SphereCollisionComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	UPoseableMeshComponent* PoseableMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	FName RotationBoneName = "body";

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	UDjabloHealthComponent* HealthComponent;
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "AI")
	UBehaviorTree* BehaviorTree;

	virtual void BeginPlay() override;

public:
	virtual void FaceRotation(FRotator NewControlRotation, float DeltaTime = 0.f) override;

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
