#include "DjabloTurretAICharacter.h"
#include <Components/SphereComponent.h>
#include <Components/PoseableMeshComponent.h>
#include "DjabloTurretAIController.h"


ADjabloTurretAICharacter::ADjabloTurretAICharacter()
{
	PrimaryActorTick.bCanEverTick = true;

	SphereCollisionComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Collision Component"));
	RootComponent = SphereCollisionComponent;

	PoseableMeshComponent = CreateDefaultSubobject<UPoseableMeshComponent>(TEXT("Mesh"));
	PoseableMeshComponent->SetupAttachment(RootComponent);

	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	AIControllerClass = ADjabloTurretAIController::StaticClass();

}

void ADjabloTurretAICharacter::BeginPlay()
{
	Super::BeginPlay();
}

void ADjabloTurretAICharacter::FaceRotation(FRotator NewControlRotation, float DeltaTime)
{
	
	auto CurrentRotation = PoseableMeshComponent->GetBoneRotationByName(RotationBoneName, EBoneSpaces::ComponentSpace);
	CurrentRotation.Yaw = NewControlRotation.Yaw;
	//NewControlRotation.Pitch = 0;
//	NewControlRotation.Roll = 0;
//	NewControlRotation.Yaw = 0;
	PoseableMeshComponent->SetBoneRotationByName(RotationBoneName, CurrentRotation, EBoneSpaces::ComponentSpace);
}

void ADjabloTurretAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	////UE_LOG(LogTemp, Error, TEXT("%s"), *PoseableMeshComponent->GetBoneRotationByName(RotationBoneName, EBoneSpaces::ComponentSpace).ToString());
	//auto CurrentRotation = PoseableMeshComponent->GetBoneRotationByName(RotationBoneName, EBoneSpaces::ComponentSpace);
	//CurrentRotation.Yaw += 1.f;

	//PoseableMeshComponent->SetBoneRotationByName(RotationBoneName, CurrentRotation, EBoneSpaces::ComponentSpace);
}

void ADjabloTurretAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

