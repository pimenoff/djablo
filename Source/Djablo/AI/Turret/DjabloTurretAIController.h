#pragma once

#include <CoreMinimal.h>
#include "DjabloAIControllerBase.h"
#include "DjabloTurretAIController.generated.h"


UCLASS()
class DJABLO_API ADjabloTurretAIController : public ADjabloAIControllerBase
{
	GENERATED_BODY()

protected:

	virtual void OnPossess(APawn* InPawn) override;

};
