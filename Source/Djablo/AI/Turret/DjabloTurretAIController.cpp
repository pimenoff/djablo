#include "DjabloTurretAIController.h"
#include <BehaviorTree/BlackboardComponent.h>
#include <DjabloTurretAICharacter.h>
#include "DjabloEnemyAIPerceptionComponent.h"


void ADjabloTurretAIController::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    if (const auto TurretCharacter = Cast<ADjabloTurretAICharacter>(InPawn)) {
        RunBehaviorTree(TurretCharacter->BehaviorTree);
    }
}