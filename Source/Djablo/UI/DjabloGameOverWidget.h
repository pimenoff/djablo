#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWidget.h"
#include "DjabloCoreTypes.h"
#include "DjabloGameOverWidget.generated.h"


class UVerticalBox;
class UButton;


UCLASS()
class DJABLO_API UDjabloGameOverWidget : public UDjabloBaseWidget
{
	GENERATED_BODY()
	
protected:
	UPROPERTY(meta = (BindWidget))
	UButton* ResetLevelButton;

	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void OnResetLevel();
};
