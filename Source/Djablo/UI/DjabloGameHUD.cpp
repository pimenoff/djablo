#include "DjabloGameHUD.h"
#include "DjabloCoreTypes.h"
#include "DjabloBaseWidget.h"
#include "DjabloGameMode.h"
#include <Kismet/GameplayStatics.h>
#include <Blueprint/UserWidget.h>


void ADjabloGameHUD::BeginPlay()
{
	Super::BeginPlay();

	GameWidgets.Add(EGameState::InProgress, CreateWidget<UDjabloBaseWidget>(GetWorld(), PlayerHUDWidgetClass));
	GameWidgets.Add(EGameState::Paused, CreateWidget<UDjabloBaseWidget>(GetWorld(), PauseWidgetClass));
	GameWidgets.Add(EGameState::GameOverLost, CreateWidget<UDjabloBaseWidget>(GetWorld(), GameOverLostWidgetClass));
	GameWidgets.Add(EGameState::GameOverWon, CreateWidget<UDjabloBaseWidget>(GetWorld(), GameOverWonWidgetClass));

	for (auto GameWidgetPair : GameWidgets) {
		const auto GameWidget = GameWidgetPair.Value;
		if (!GameWidget) {
			continue;
		}

		GameWidget->AddToViewport();
		GameWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	if (GetWorld()) {
		const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
		if (GameMode) {
			GameMode->OnGameStateChanged.AddUObject(this, &ADjabloGameHUD::OnGameStateChanged);
		}
	}
}

void ADjabloGameHUD::OnGameStateChanged(EGameState GameState)
{
	if (CurrentWidget) {
		CurrentWidget->SetVisibility(ESlateVisibility::Hidden);
	}

	if (GameWidgets.Contains(GameState)) {
		CurrentWidget = GameWidgets[GameState];
	}

	if (CurrentWidget) {
		CurrentWidget->SetVisibility(ESlateVisibility::Visible);
		CurrentWidget->Show();
	}
}
