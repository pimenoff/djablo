#pragma once

#include <CoreMinimal.h>
#include "DjabloBaseWidget.h"
#include "DjabloPauseWidget.generated.h"


class UButton;


UCLASS()
class DJABLO_API UDjabloPauseWidget : public UDjabloBaseWidget
{
	GENERATED_BODY()

protected:
	UPROPERTY(meta = (BindWidget))
	UButton* ClearPauseButton;

	virtual void NativeOnInitialized() override;

private:
	UFUNCTION()
	void OnClearPause();
};
