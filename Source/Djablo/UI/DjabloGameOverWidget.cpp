#include "DjabloGameOverWidget.h"
#include <Components/VerticalBox.h>
#include <Components/Button.h>
#include <Kismet/GameplayStatics.h>


void UDjabloGameOverWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (ResetLevelButton) {
		ResetLevelButton->OnClicked.AddDynamic(this, &UDjabloGameOverWidget::OnResetLevel);
	}
}

void UDjabloGameOverWidget::OnResetLevel()
{
	const FString CurrentLevelName = UGameplayStatics::GetCurrentLevelName(this);
	UGameplayStatics::OpenLevel(this, FName(CurrentLevelName));
}
