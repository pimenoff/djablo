#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>
#include "DjabloBaseWidget.generated.h"


class USoundCue;


UCLASS()
class DJABLO_API UDjabloBaseWidget : public UUserWidget
{
	GENERATED_BODY()

public:
	void Show();

protected:
	//UPROPERTY(meta = (BindWidgetAnim), Transient)
	//UWidgetAnimation* ShowAnimation;
	//
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* OpenSound;
};
