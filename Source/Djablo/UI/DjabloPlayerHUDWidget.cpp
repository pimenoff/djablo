#include "DjabloPlayerHUDWidget.h"
#include "DjabloHealthComponent.h"
#include "DjabloWeaponComponent.h"
#include "DjabloPlayerCharacter.h"
#include "DjabloBaseWeapon.h"
#include "DjabloGameMode.h"
#include <Components/ProgressBar.h>


float UDjabloPlayerHUDWidget::GetHealthPercentage() const
{
	const auto Player = GetOwningPlayerPawn();
	if (!Player) {
		return 0.f;
	}

	const auto Comp = Player->GetComponentByClass(UDjabloHealthComponent::StaticClass());
	const auto HealthComponent = Cast<UDjabloHealthComponent>(Comp);

	if (!HealthComponent) {
		return 0.f;
	}

	return HealthComponent->GetHealthPercentage();
}

FName UDjabloPlayerHUDWidget::GetCurrentWeaponName() const
{
	const auto Player = GetOwningPlayerPawn();
	if (!Player) {
		return FName();
	}

	const auto Comp = Player->GetComponentByClass(UDjabloWeaponComponent::StaticClass());
	const auto WeaponComponent = Cast<UDjabloWeaponComponent>(Comp);

	if (!WeaponComponent) {
		return FName();
	}

	return UEnum::GetValueAsName(WeaponComponent->CurrentWeaponName);
}

bool UDjabloPlayerHUDWidget::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{
	const auto Player = GetOwningPlayerPawn();
	if (!Player) {
		return false;
	}

	const auto Comp = Player->GetComponentByClass(UDjabloWeaponComponent::StaticClass());
	const auto WeaponComponent = Cast<UDjabloWeaponComponent>(Comp);

	if (!WeaponComponent) {
		return false;
	}

	return WeaponComponent->GetCurrentWeaponAmmoData(AmmoData);
}

int32 UDjabloPlayerHUDWidget::GetKillsNum() const
{
	const auto Controller = GetOwningPlayer();
	if (!Controller) {
		return 0;
	}

	const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
	return GameMode ? GameMode->KillsNum : 0;
}

FString UDjabloPlayerHUDWidget::GetLevelCompletenessPercentage() const
{
	const auto GameMode = Cast<ADjabloGameMode>(GetWorld()->GetAuthGameMode());
	if (!GameMode || !GameMode->TotalEnemiesOnLevel) {
		return FString();
	}

	return FString::Printf(TEXT("%d"), GameMode->KillsNum * 100 / GameMode->TotalEnemiesOnLevel).Append("%");
}

FString UDjabloPlayerHUDWidget::FormatBullets(int32 BulletsNum) const
{
	const int32 MaxLen = 2;
	const TCHAR PrefixSymbol = '0';

	auto BulletStr = FString::FromInt(BulletsNum);
	const auto SymbolsNumToAdd = MaxLen - BulletStr.Len();

	if (SymbolsNumToAdd > 0) {
		BulletStr = FString::ChrN(SymbolsNumToAdd, PrefixSymbol).Append(BulletStr);
	}

	return BulletStr;
}

bool UDjabloPlayerHUDWidget::Initialize()
{
	const auto Player = GetOwningPlayerPawn();
	if (Player) {
		const auto Comp = Player->GetComponentByClass(UDjabloHealthComponent::StaticClass());
		const auto HealthComponent = Cast<UDjabloHealthComponent>(Comp);

		if (HealthComponent) {
			HealthComponent->OnHealthChanged.AddUObject(this, &UDjabloPlayerHUDWidget::OnHealthChanged);
		}
	}

	return Super::Initialize();
}

void UDjabloPlayerHUDWidget::OnHealthChanged(float Health, float HealthDelta)
{
	if (HealthDelta < 0.0f) {
		OnTakeDamage();

		if (!IsAnimationPlaying(DamageAnimation)) {
			PlayAnimation(DamageAnimation);
		}
	}

	UpdateHealthBar();
}

void UDjabloPlayerHUDWidget::UpdateHealthBar()
{
	if (HealthProgressBar) {
		HealthProgressBar->SetFillColorAndOpacity(GetHealthPercentage() > PersentColorThreshold ? GoodColor : BadColor);
	}
}
