#pragma once

#include <CoreMinimal.h>
#include <GameFramework/HUD.h>
#include "DjabloCoreTypes.h"
#include "DjabloGameHUD.generated.h"


class UDjabloBaseWidget;


UCLASS()
class DJABLO_API ADjabloGameHUD : public AHUD
{
	GENERATED_BODY()

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PlayerHUDWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> PauseWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> GameOverLostWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	TSubclassOf<UUserWidget> GameOverWonWidgetClass;

	virtual void BeginPlay() override;

private:

	UPROPERTY()
	TMap<TEnumAsByte<EGameState>, UDjabloBaseWidget*> GameWidgets;

	UPROPERTY()
	UDjabloBaseWidget* CurrentWidget = nullptr;

	void OnGameStateChanged(EGameState GameState);
};
