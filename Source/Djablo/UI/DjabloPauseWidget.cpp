#include "DjabloPauseWidget.h"
#include <GameFramework/GameModeBase.h>
#include <Components/Button.h>


void UDjabloPauseWidget::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	if (ClearPauseButton) {
		ClearPauseButton->OnClicked.AddDynamic(this, &UDjabloPauseWidget::OnClearPause);
	}
}

void UDjabloPauseWidget::OnClearPause()
{
	if (!GetWorld() || !GetWorld()->GetAuthGameMode()) {
		return;
	}

	GetWorld()->GetAuthGameMode()->ClearPause();
}
