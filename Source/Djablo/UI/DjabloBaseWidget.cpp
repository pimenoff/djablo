#include "DjabloBaseWidget.h"
#include <Sound/SoundCue.h>
#include <Kismet/GameplayStatics.h>


void UDjabloBaseWidget::Show()
{
	//if (ShowAnimation) {
	//	PlayAnimation(ShowAnimation);
	//}

	if (OpenSound) {
		UGameplayStatics::PlaySound2D(GetWorld(), OpenSound);
	}
}
