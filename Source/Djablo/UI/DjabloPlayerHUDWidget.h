#pragma once

#include <CoreMinimal.h>
#include <Blueprint/UserWidget.h>
#include "DjabloBaseWidget.h"
#include "DjabloPlayerHUDWidget.generated.h"


class UProgressBar;


UCLASS()
class DJABLO_API UDjabloPlayerHUDWidget : public UDjabloBaseWidget
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, Category = "UI")
	float GetHealthPercentage() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	FName GetCurrentWeaponName() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;

	UFUNCTION(BlueprintImplementableEvent, Category = "UI")
	void OnTakeDamage() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	int32 GetKillsNum() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	FString GetLevelCompletenessPercentage() const;

	UFUNCTION(BlueprintCallable, Category = "UI")
	FString FormatBullets(int32 BulletsNum) const;

	virtual bool Initialize() override;

protected:
	UPROPERTY(meta = (BindWidget))
	UProgressBar* HealthProgressBar;

	UPROPERTY(meta = (BindWidgetAnim), Transient)
	UWidgetAnimation* DamageAnimation;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	float PersentColorThreshold = 0.3f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	FLinearColor GoodColor = FLinearColor::White;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "UI")
	FLinearColor BadColor = FLinearColor::Red;

private:
	void OnHealthChanged(float Health, float HealthDelta);
	void UpdateHealthBar();
};
