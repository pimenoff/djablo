#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>
#include "DjabloCoreTypes.h"
#include "DjabloHealthComponent.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class DJABLO_API UDjabloHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDjabloHealthComponent();

	FOnDeathSignature OnDeath;
	FOnHealthChangedSignature OnHealthChanged;

	UFUNCTION(BlueprintCallable, Category = "Health")
	float GetHealthPercentage() const { return Health / MaxHealth; }

	float GetHealth() const { return Health; }
	bool IsDead() const { return Health <= 0; }
	bool TryToAddHealth(float HealthAmount);
	bool IsHealthFull() const;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = "0.1"))
	float MaxHealth = 1000.f;

	virtual void BeginPlay() override;

private:
	float Health;

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);

	UFUNCTION()
	void OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser);

	UFUNCTION()
	void OnTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, const FHitResult& HitInfo, class AController* InstigatedBy, AActor* DamageCauser);

	void SetHealth(float NewHealth);
	void ApplyDamage(float Damage, AController* InstigatedBy);
	void ReportDamageEvent(float Damage, AController* InstigatedBy);
};
