#include "DjabloHealthComponent.h"
#include <Perception/AISense_Damage.h>


UDjabloHealthComponent::UDjabloHealthComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

void UDjabloHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	Health = MaxHealth;

	AActor* Owner = GetOwner();
	if (Owner) {
		Owner->OnTakeAnyDamage.AddDynamic(this, &UDjabloHealthComponent::OnTakeAnyDamage);
		Owner->OnTakePointDamage.AddDynamic(this, &UDjabloHealthComponent::OnTakePointDamage);
		Owner->OnTakeRadialDamage.AddDynamic(this, &UDjabloHealthComponent::OnTakeRadialDamage);
	}
}

void UDjabloHealthComponent::ApplyDamage(float Damage, AController* InstigatedBy)
{
	if (Damage <= 0.0f || IsDead()) {
		return;
	}

	SetHealth(Health - Damage);

	if (IsDead()) {
		OnDeath.Broadcast();
	}

	ReportDamageEvent(Damage, InstigatedBy);
}

void UDjabloHealthComponent::SetHealth(float NewHealth)
{
	const auto NextHealth = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
	const auto HealthDelta = NextHealth - Health;

	Health = NextHealth;
	OnHealthChanged.Broadcast(Health, HealthDelta);
}

bool UDjabloHealthComponent::TryToAddHealth(float HealthAmount)
{
	if (IsDead() || IsHealthFull()) return false;
	SetHealth(Health + HealthAmount);
	return true;
}

bool UDjabloHealthComponent::IsHealthFull() const
{
	return FMath::IsNearlyEqual(Health, MaxHealth);
}

void UDjabloHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType, AController* InstigatedBy, AActor* DamageCauser)
{
	ApplyDamage(Damage, InstigatedBy);
}

void UDjabloHealthComponent::OnTakePointDamage(AActor* DamagedActor, float Damage, class AController* InstigatedBy, FVector HitLocation, class UPrimitiveComponent* FHitComponent, FName BoneName, FVector ShotFromDirection, const class UDamageType* DamageType, AActor* DamageCauser)
{
	ApplyDamage(Damage, InstigatedBy);
}

void UDjabloHealthComponent::OnTakeRadialDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, FVector Origin, const FHitResult& HitInfo, class AController* InstigatedBy, AActor* DamageCauser)
{
	ApplyDamage(Damage, InstigatedBy);
}

void UDjabloHealthComponent::ReportDamageEvent(float Damage, AController* InstigatedBy)
{
	if (!InstigatedBy || !InstigatedBy->GetPawn() || !GetOwner()) {
		return;
	}

	UAISense_Damage::ReportDamageEvent(
		GetWorld(),
		GetOwner(),
		InstigatedBy->GetPawn(),
		Damage,
		InstigatedBy->GetPawn()->GetActorLocation(),
		GetOwner()->GetActorLocation()
	);
}