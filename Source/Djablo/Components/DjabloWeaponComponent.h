#pragma once

#include <CoreMinimal.h>
#include <Components/ActorComponent.h>
#include "DjabloBaseWeapon.h"
#include "DjabloCoreTypes.h"
#include "DjabloWeaponComponent.generated.h"


class ADjabloPistolWeapon;
class ADjabloRifleWeapon;
class ADjabloShotgunWeapon;
class ADjabloGrenadeLauncherWeapon;


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class DJABLO_API UDjabloWeaponComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	UDjabloWeaponComponent();

	virtual void StartFire();
	void StopFire();
	void Reload();

	bool GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const;
	bool TryToAddAmmo(TSubclassOf<ADjabloBaseWeapon> WeaponType, int32 ClipsAmount);
	bool NeedAmmo(TSubclassOf<ADjabloBaseWeapon> WeaponType);
	void EquipWeapon(EWeaponName WeaponName);

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Weapon")
	ADjabloBaseWeapon* CurrentWeapon = nullptr;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = "Weapon")
	TEnumAsByte<EWeaponName> CurrentWeaponName;

protected:
	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	TMap<TEnumAsByte<EWeaponName>, FWeaponData> WeaponData;

	UPROPERTY(EditDefaultsOnly, Category = "Weapon")
	FName WeaponSocketName = "RightHandWeaponSocket";

	UPROPERTY()
	TMap<TEnumAsByte<EWeaponName>, ADjabloBaseWeapon*> Weapons;

	bool CanFire() const;

	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason);

private:
	UPROPERTY()
	UAnimMontage* CurrentReloadAnimMontage = nullptr;

	TArray<TEnumAsByte<EWeaponName>> HaveWeapons = { EWeaponName::Pistol, EWeaponName::Rifle, EWeaponName::Shotgun, EWeaponName::GrenadeLauncher };

	bool ReloadAnimInProgress = false;

	void SpawnWeapons();
	void AttachWeaponToSocket(ADjabloBaseWeapon* Weapon, USceneComponent* SceneComponent, const FName& SocketName);

	void PlayAnimMontage(UAnimMontage* Animation);
	void InitAnimations();

	void OnReloadFinished(USkeletalMeshComponent* MeshComp);

	bool CanReload() const;

	void OnEmptyClip(ADjabloBaseWeapon* AmmoEmptyWeapon);
	void ChangeClip();
};
