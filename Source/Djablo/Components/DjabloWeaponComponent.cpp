#include "DjabloWeaponComponent.h"
#include "DjabloBaseWeapon.h"
#include "DjabloPistolWeapon.h"
#include "DjabloRifleWeapon.h"
#include "DjabloShotgunWeapon.h"
#include "DjabloGrenadeLauncherWeapon.h"
#include "DjabloReloadFinishedAnimNotify.h"
#include "AnimUtils.h"
#include <GameFramework/Character.h>


UDjabloWeaponComponent::UDjabloWeaponComponent()
{
	PrimaryComponentTick.bCanEverTick = true;
}

bool UDjabloWeaponComponent::CanFire() const
{
	return CurrentWeapon && !ReloadAnimInProgress;
}

void UDjabloWeaponComponent::EquipWeapon(EWeaponName WeaponName)
{
	if (! Weapons.Contains(WeaponName)) {
		return;
	}

	if (!HaveWeapons.Contains(WeaponName)) {
		return;
	}

	if (ReloadAnimInProgress) {
		return;
	}

	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character) {
		return;
	}

	if (CurrentWeapon) {
		CurrentWeapon->StopFire();
		CurrentWeapon->DetachRootComponentFromParent(false);
	}

	CurrentWeaponName = WeaponName;
	CurrentWeapon = Weapons[WeaponName];
	CurrentReloadAnimMontage = WeaponData.Contains(WeaponName) ? WeaponData[WeaponName].ReloadAnimMontage : nullptr;

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, false);
	CurrentWeapon->AttachToComponent(Character->GetMesh(), AttachmentRules, WeaponSocketName);
}

void UDjabloWeaponComponent::BeginPlay()
{
	Super::BeginPlay();

	SpawnWeapons();
	EquipWeapon(EWeaponName::Pistol);
	InitAnimations();
}

void UDjabloWeaponComponent::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	CurrentWeapon = nullptr;
	for (auto& Weapon : Weapons)
	{
		Weapon.Value->DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
		Weapon.Value->Destroy();
	}

	Weapons.Empty();
	Super::EndPlay(EndPlayReason);
}

void UDjabloWeaponComponent::SpawnWeapons()
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character || !GetWorld()) {
		return;
	}

	for (auto OneWeaponData : WeaponData) {
		auto Weapon = GetWorld()->SpawnActor<ADjabloBaseWeapon>(OneWeaponData.Value.WeaponClass);
		if (!Weapon) {
			continue;
		}

		Weapon->OnClipsEmpty.AddUObject(this, &UDjabloWeaponComponent::OnEmptyClip);
		Weapon->SetOwner(Character);
		Weapons.Emplace(OneWeaponData.Key, Weapon);
	}
}

void UDjabloWeaponComponent::AttachWeaponToSocket(ADjabloBaseWeapon* Weapon, USceneComponent* SceneComponent, const FName& SocketName)
{
	if (!Weapon || !SceneComponent) {
		return;
	}

	FAttachmentTransformRules AttachmentRule(EAttachmentRule::SnapToTarget, false);
	Weapon->AttachToComponent(SceneComponent, AttachmentRule, SocketName);
}

void UDjabloWeaponComponent::PlayAnimMontage(UAnimMontage* Animation)
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character) {
		return;
	}

	Character->PlayAnimMontage(Animation);
}

void UDjabloWeaponComponent::InitAnimations()
{
	for (auto OneWeaponData : WeaponData) {
		auto ReloadFinishedNotify = AnimUtils::FindNotifyByClass<UDjabloReloadFinishedAnimNotify>(OneWeaponData.Value.ReloadAnimMontage);

		if (ReloadFinishedNotify) {
			ReloadFinishedNotify->OnNotified.AddUObject(this, &UDjabloWeaponComponent::OnReloadFinished);
		}
	}
}

void UDjabloWeaponComponent::OnReloadFinished(USkeletalMeshComponent* MeshComp)
{
	ACharacter* Character = Cast<ACharacter>(GetOwner());
	if (!Character || Character->GetMesh() != MeshComp) {
		return;
	}

	ReloadAnimInProgress = false;
}

void UDjabloWeaponComponent::StartFire()
{
	if (!CurrentWeapon) {
		return;
	}

	if (ReloadAnimInProgress) {
		return;
	}

	if (CurrentWeapon->IsAmmoEmpty()) {
		return;
	}

	CurrentWeapon->StartFire();
}

void UDjabloWeaponComponent::StopFire()
{
	if (!CurrentWeapon) {
		return;
	}

	CurrentWeapon->StopFire();
}

void UDjabloWeaponComponent::Reload()
{
	ChangeClip();
}

bool UDjabloWeaponComponent::GetCurrentWeaponAmmoData(FAmmoData& AmmoData) const
{
	if (CurrentWeapon) {
		AmmoData = CurrentWeapon->GetAmmoData();
		return true;
	}

	return false;
}

bool UDjabloWeaponComponent::TryToAddAmmo(TSubclassOf<ADjabloBaseWeapon> WeaponType, int32 ClipsAmount)
{
	for (const auto Weapon : Weapons) {
		if (Weapon.Value && Weapon.Value->IsA(WeaponType)) {
			return Weapon.Value->TryToAddAmmo(ClipsAmount);
		}
	}

	return false;
}

bool UDjabloWeaponComponent::NeedAmmo(TSubclassOf<ADjabloBaseWeapon> WeaponType)
{
	for (const auto Weapon : Weapons) {
		if (Weapon.Value && Weapon.Value->IsA(WeaponType)) {
			return !Weapon.Value->IsAmmoFull();
		}
	}

	return false;
}

bool UDjabloWeaponComponent::CanReload() const
{
	return CurrentWeapon
		&& !ReloadAnimInProgress
		&& CurrentWeapon->CanReload();
}

void UDjabloWeaponComponent::OnEmptyClip(ADjabloBaseWeapon* AmmoEmptyWeapon)
{
	if (!AmmoEmptyWeapon) {
		return;
	}

	if (CurrentWeapon == AmmoEmptyWeapon) {
		ChangeClip();
	} else {
		for (const auto Weapon : Weapons) {
			if (Weapon.Value == AmmoEmptyWeapon) {
				Weapon.Value->ChangeClip();
			}
		}
	}
}

void UDjabloWeaponComponent::ChangeClip()
{
	if (!CanReload()) {
		return;
	}

	CurrentWeapon->StopFire();
	CurrentWeapon->ChangeClip();
	ReloadAnimInProgress = true;

	PlayAnimMontage(CurrentReloadAnimMontage);
}
