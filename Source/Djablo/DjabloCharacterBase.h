#pragma once

#include <CoreMinimal.h>
#include <GameFramework/Character.h>
#include "DjabloCharacterBase.generated.h"


class UDjabloHealthComponent;
class USoundCue;


UCLASS()
class DJABLO_API ADjabloCharacterBase : public ACharacter
{
	GENERATED_BODY()

public:
	ADjabloCharacterBase(const FObjectInitializer& ObjInit);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Components)
	UDjabloHealthComponent* HealthComponent;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Sound")
	USoundCue* DeathSound;

	virtual void BeginPlay() override;
	virtual void OnDeath();
	virtual void FinalizeDeath() {};
	virtual void OnHealthChanged(float Health, float HealthDelta);
};
